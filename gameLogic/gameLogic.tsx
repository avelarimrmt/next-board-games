import db from "../config/firebase";
import {
    takeACard,
    isReverse,
    isSkip,
    isWildDrawFour,
    isDrawTwo, isWild,
} from "../utils/game";
import {cards} from "../utils/cards";

export function yellOne(player, roomId, playersActive, room) {
    const roomRef = db.collection("rooms").doc(roomId);
    const playerCards = playersActive[room.currentMove].data().cards;
    let pennalty;
    if (playerCards.length > 2) {
        // добавляются 4 карты, если не нажали уно
        pennalty = 4;
    } else {
        pennalty = null;
    }

    roomRef.set(
        {
            yellOne: player,
            pennalty: pennalty,
        },
        {merge: true}
    );
}

export function getPlayingCards(playersActive, room) {
    const cards = [];
    playersActive.forEach((player) => {
        cards.push(...player.data().cards);
    });
    cards.push(room.discardPile);
    return cards;
}

export function verifyYellPlayer(room) {
    let yellOne;
    if (room.currentMove === room.yellOne) {
        return (yellOne = room.yellOne);
    } else {
        return (yellOne = null);
    }
}

export function passTurn(player, roomId, room, playersActive) {
    const roomRef = db.collection("rooms").doc(roomId);

    const totalPlayers = playersActive.length;

    const moves = 1;
    const roomIsReverse = room.isReverse;
    const direction = roomIsReverse ? -1 : 1;

    const nextPlayer = (totalPlayers + (room.currentMove + moves * direction)) % totalPlayers;

    const playerCards = playersActive[room.currentMove].data().cards;
    const playingCards = getPlayingCards(playersActive, room);
    const usedCards = room.deckDict;

    let pennalty = room.pennalty;
    if (pennalty > 0) {
        for (let i = 0; i < pennalty; i++) {
            const newCard = takeACard(usedCards, playingCards);
            playerCards.push(newCard);
            playingCards.push(newCard);
        }
    }

    playersActive[player].ref.set(
        {
            cards: playerCards,
        },
        {merge: true}
    );

    roomRef.set(
        {
            currentMove: nextPlayer,
            deckDict: usedCards,
            previousMove: player,
            yellOne: null,
            drawCount: 0,
            drawPile: false,
            pennalty: null,
        },
        {merge: true}
    );
}

export function drawCard(room, playersActive, roomId) {
    const player = room.currentMove;
    const usedCards = room.deckDict;
    const playingCards = getPlayingCards(playersActive, room);
    let playerCards = playersActive[player].data().cards;
    let drawCount = room.drawCount;

    let pennalty = room.pennalty;
    let total;
    if (pennalty) {
        total = drawCount + pennalty;
    } else {
        total = drawCount;
    }

    if (drawCount > 0 || pennalty) {
        for (let i = 0; i < total; i++) {
            const newCard = takeACard(usedCards, playingCards);
            playerCards.push(newCard);
            playingCards.push(newCard);
        }
    } else {
        const card = takeACard(usedCards, playingCards);
        playingCards.push(card);
        playerCards.push(card);
    }

    playersActive[player].ref.set(
        {
            cards: playerCards,
        },
        {merge: true}
    );

    const roomRef = db.collection("rooms").doc(roomId);

    if (drawCount > 0) {
        const totalPlayers = playersActive.length;
        const moves = 1;
        const roomIsReverse = room.isReverse;
        const direction = roomIsReverse ? -1 : 1;

        const nextPlayer = (totalPlayers + (room.currentMove + moves * direction)) % totalPlayers;

        drawCount = 0;

        roomRef.set(
            {
                deckDict: usedCards,
                yellOne: null,
                drawCount: drawCount,
                currentMove: nextPlayer,
                previousMove: player,
                drawPile: false,
                pennalty: null,
            },
            {merge: true}
        );
    } else {
        roomRef.set(
            {
                deckDict: usedCards,
                yellOne: null,
                drawCount: drawCount,
                drawPile: true,
                pennalty: null,
            },
            {merge: true}
        );
    }
}

export function sameCards(
    card, cardPile,
    playersActive, room) {
    const playerCards = playersActive[room.currentMove].data().cards;

    const indexNewCard = card - 1;
    const newCards = cards[indexNewCard];
    const indexCardPile = cardPile - 1;
    const pileCards = cards[indexCardPile];
    let isSame = false;
    /*console.log(cardPile);
    console.log(newCards);*/

    if ((newCards.score !== 50) && (newCards.score !== 20) && (newCards.number != null) && (newCards.number === pileCards.number) || (newCards.color === pileCards.color)) {
        playerCards.forEach((card) => {
            if ((card - 1 !== indexNewCard) && cards[card - 1].number === newCards.number && cards[card - 1].color === newCards.color)
                isSame = true;
        });
    }
    return isSame;
}

export function discardACard(roomId, playersActive, card, color, room, count = null) {
    const roomRef = db.collection("rooms").doc(roomId);
    const totalPlayers = playersActive.length;
    let moves;
    let roomIsReverse;
    if (totalPlayers === 2 && isReverse(card)) {
        moves = 2;
        roomIsReverse = room.isReverse;
    } else {
        roomIsReverse = isReverse(card) ? !room.isReverse : room.isReverse;
        moves = isSkip(card) ? 2 : 1;
    }
    const direction = roomIsReverse ? -1 : 1;
    const playerCards = playersActive[room.currentMove].data().cards;
    const winners = room.winners;

    const nextPlayer = (totalPlayers + (room.currentMove + moves * direction)) % totalPlayers;


    let drawCount = room.drawCount || 0;
    let accum = room.accum;
    if (accum) {
        if (isWildDrawFour(card)) {
            drawCount += 4;
        } else if (isDrawTwo(card)) {
            drawCount += 2;
        }
    } else {
        if (isWildDrawFour(card)) {
            drawCount = 4;
        } else if (isDrawTwo(card)) {
            drawCount = 2;
        }
    }

    let nextCards = playerCards.filter((c) => c !== card);

    let usedCards = room.deckDict;
    let yellOne = verifyYellPlayer(room);
    let pennalty = room.pennalty;
    const playingCards = getPlayingCards(playersActive, room);
    if (yellOne == null && nextCards.length === 1) {
        pennalty = 4;
    }
    if (pennalty > 0) {
        for (let i = 0; i < pennalty; i++) {
            const newCard = takeACard(usedCards, playingCards);
            nextCards.push(newCard);
            playingCards.push(newCard);
        }
    }
    const prevDiscardCard = room.discardPile;
    let isSame;
    if (!isWildDrawFour(card) && !isWild(card) && !isDrawTwo(card) && !isReverse(card) && !isSkip(card))
        isSame = sameCards(card, prevDiscardCard, playersActive, room);

    if (isSame && room.dropSameCards) {
        playersActive[room.currentMove].ref.set(
            {
                cards: nextCards,
            },
            {merge: true}
        );

        roomRef.set(
            {
                deckDict: usedCards,
                discardPile: card,
                discardColor: color || null,
                isReverse: roomIsReverse,
                yellOne: yellOne,
                drawCount: drawCount,
                drawPile: false,
                pennalty: null,
                isSame: true,
            },
            {merge: true}
        );
    } else {
        playersActive[room.currentMove].ref.set(
            {
                cards: nextCards,
            },
            {merge: true}
        );
        if (nextCards.length === 0) {
            playersActive[room.currentMove].ref.set(
                {
                    winner: true,
                },
                {merge: true}
            );

            winners.push(new Object( {id: playersActive[room.currentMove].id, name: playersActive[room.currentMove].data().name, avatar: playersActive[room.currentMove].data().avatar}));

            roomRef.set(
                {
                    winners: winners,
                },
                {merge: true}
            );

            if (room.currentMove === playersActive.length - 1)
            {
                if (!roomIsReverse)
                    roomRef.set({
                        previousMove: room.previousMove,
                        currentMove: 0,
                    }, {merge: true});
                else
                    roomRef.set({
                        previousMove: 0,
                        currentMove: room.currentMove - 1,
                    }, {merge: true});
            }
            else {
                if (roomIsReverse) {
                    if (room.currentMove === 0)
                        roomRef.set({
                            previousMove: 0,
                            currentMove: playersActive.length - 2,
                        }, {merge: true});
                    else
                        roomRef.set({
                            previousMove: room.previousMove - 1,
                            currentMove: room.currentMove - 1,
                        }, {merge: true});
                }
                else
                    if (nextPlayer !== playersActive.length - 1)
                        roomRef.set({
                            currentMove: nextPlayer,
                            previousMove: room.currentMove,
                        }, {merge: true});
                    else
                        roomRef.set({
                            currentMove: 0,
                            previousMove: room.currentMove,
                        }, {merge: true});
            }

            roomRef.set(
                {
                    deckDict: usedCards,
                    discardPile: card,
                    discardColor: color || null,
                    isReverse: roomIsReverse,
                    yellOne: yellOne,
                    drawCount: drawCount,
                    drawPile: false,
                    pennalty: null,
                },
                {merge: true}
            );
        }
        else {
            roomRef.set(
                {
                    deckDict: usedCards,
                    currentMove: nextPlayer,
                    previousMove: room.currentMove,
                    discardPile: card,
                    discardColor: color || null,
                    isReverse: roomIsReverse,
                    yellOne: yellOne,
                    drawCount: drawCount,
                    drawPile: false,
                    pennalty: null,
                },
                {merge: true}
            );
        }
    }

    if (count !== null) {
        roomRef.set(
            {
                isSame: false,
                countSame: 0,
            },
            {merge: true}
        );
    }
}
