import React from "react";

export const goalOfGameTitle = (
    <span>
        Цель игры
    </span>
);

export const goalOfGameTxt = (
    <span>
        первым избавиться от всех карт.
    </span>
);

export const descOfGameTitle = (
    <span>
        Описание карт
    </span>
);

export const ordinaryCardTxt = (
    <span>
        Есть обычные карты 4 разных цветов (красный, желтый, зеленый и синий) с цифрами
от 0 до 9.
    </span>
);

export const actionCardTxt = (
    <span>
        Кроме обычных карт есть активные карты.
    </span>
);

export const actionCardInDiscardTxt = (
    <span>
        Эти карты можно использовать только если они совпадают с цветом верхней
карты в сбросе:
    </span>
);

export const drawTwoTitle = (
    <span>Возьми две —</span>
);

export const drawTwoTxt = (
    <span>следующий по направлению игры игрок берёт 2 карты из колоды банка <br/>и пропускает свой ход.</span>
);

export const reverseTitle = (
    <span>Ход обратно —</span>
);

export const reverseTxt = (
    <span>направление хода меняется.</span>
);

export const skipTitle = (
    <span>Пропусти ход —</span>
);

export const skipTxt = (
    <span>следующий игрок по направлению игры пропускает ход.</span>
);

export const wildCarsTxt = (
    <span>Эти черные карты можно использовать, независимо от цвета верхней карты:</span>
);

export const wildCardTitle = (
    <span>Закажи цвет —</span>
);

export const wildCardTxt = (
    <span>игрок заказывает цвет и следующий игрок по направлению игры должен сделать ход либо
        картой заказанного цвета, либо карту на чёрном фоне.</span>
);

export const wildFourTitle = (
    <span>Возьми четыре —</span>
);

export const wildFourTxt = (
    <span>игрок заказывает цвет, следующий игрок по направлению берёт 4 карты из колоды и пропускает ход.</span>
);

export const howToMoveTitle = (
    <span>Как ходить?</span>
);

export const howToMoveTxt = (
    <span>Каждый игрок получает по 7 карт. Участники по очереди выкладывают по одной карте,
        которая имеет тот же цвет или ту же цифру, что и карта предыдущего ходящего. Если у
        Вас нет подходящей карты, пользуйтесь своими активными картами... Или тяните карту
        из колоды. Если вытянутая карта тоже не подходит, то оставляете ее себе и
        пропускаете ход.</span>
);

export const exampleTitle = (
    <span>ПРИМЕР:</span>
);

export const exampleTxt = (
    <span>Если верхняя карта – зеленая 2, игрок должен положить сверху зеленую карту
        ИЛИ 2 любого цвета.</span>
);

export const howToWinTitle = (
    <span>Как выигрывать?</span>
);

export const howToWinTxt = (
    <span>Нужно первым избавиться от всех карт. Перед выкладыванием на стол предпоследней
        карты нужно нажать «UNO». Если забудете – получите штраф из четырех карт.</span>
);

export const scoringTitle = (
    <span>Подсчет баллов</span>
);

export const scoringTxt = (
    <span>Когда игра заканчивается, идёт подсчёт очков по оставшимся на руках у игроков карт.</span>
);

export const andFurtherTitle = (
    <span>И еще...</span>
);

export const andFurtherTxt = (
    <span>Для каждой игры доступна настройка правил. Посмотреть такие правила настроены для
        следующей партии можно в Правилах комнаты. Настраивает эти правила игрок, первый
        зашедшим в комнату – он отмечен звездочкой.</span>
);