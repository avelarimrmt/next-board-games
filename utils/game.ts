import {cards} from "./cards";
import db from "../config/firebase";

const cardsCount = cards.length;

export function range() {
    let deck = [];
    for (let i = 1; i <= cardsCount; i++) {
        deck.push(i);
    }
    return deck;
}

export function takeACard(usedCards, playingCards = null) {
    let random_card = Math.floor(Math.random() * cardsCount);

    const deck = range();
    const card = deck[random_card];

    if (!usedCards[card]) {
        usedCards[card] = true;
        return card;
    } else if (
        Object.keys(usedCards).filter((card) => usedCards[card]).length === 108
    ) {
        console.log("Больше нет карт");

        Object.keys(usedCards).forEach((key) => {
            usedCards[key] = false;
        });

        playingCards.forEach((card) => (usedCards[card] = true));

        return takeACard(usedCards, playingCards);
    } else {
        return takeACard(usedCards, playingCards);
    }
}

export function isAllowedToThrow(
    newCard,
    cardPile,
    color,
    drawCount,
    playerCards,
    room = null,
) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    const indexCardPile = cardPile - 1;
    const pileCards = cards[indexCardPile];
    const accum = room.accum;
    const discardColor = room.discardColor;
    const wildFourDifficulty = room.wildFourDifficulty;
    if (drawCount > 0) {
        if (accum) {
            return (pileCards.special === "wild-drawFour" &&
                newCards.special === "wild-drawFour") ||
                (pileCards.special === "drawTwo" && newCards.special === "drawTwo") ||
                (pileCards.special === "wild-drawFour" && discardColor === newCards.color && newCards.special === "drawTwo")
                || (pileCards.special === "drawTwo" && newCards.special === "wild-drawFour");
        } else {
            return false;
        }

    } else if (newCards.special === "wild-drawFour") {
        return !playerCards.find((card) => {
            if (wildFourDifficulty) {
                if (pileCards.color) {
                    return cards[card - 1].color === pileCards.color;
                } else if (color) {
                    return cards[card - 1].color === color;
                }
            } else return false;
        });
    } else
        return (
            (newCards.number != null && newCards.number === pileCards.number) ||
            newCards.color === pileCards.color ||
            ((pileCards.special === "wild" || pileCards.special === "wild-drawFour") &&
                newCards.color === color) ||
            (newCards.special != null && newCards.special === pileCards.special) ||
            newCards.special === "wild"
        );
}

export function isAllowedToThrowSame(
    newCard,
    cardPile,
    color,
    drawCount,
    playerCards,
    room = null,
    playersActive,
    roomId,) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    const indexCardPile = cardPile - 1;
    const pileCards = cards[indexCardPile];
    const roomRef = db.collection("rooms").doc(roomId);
        if (room.isSame)
        {roomRef.set(
            {
                countSame: 1,
            },
            {merge: true}
        );
        return (newCards.number != null && newCards.number === pileCards.number && newCards.color === pileCards.color);}
}

export function isReverse(newCard) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    return newCards.special === "reverse";
}

export function isSkip(newCard) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    return newCards.special === "skip";
}

export function isWild(newCard) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    return newCards.special === "wild" || newCards.special === "wild-drawFour";
}

export function isWildDrawFour(newCard) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    return newCards.special === "wild-drawFour";
}

export function isDrawTwo(newCard) {
    const indexNewCard = newCard - 1;
    const newCards = cards[indexNewCard];
    return newCards.special === "drawTwo";
}

const colorOrderMap = {
    blue: 1,
    green: 2,
    red: 3,
    yellow: 4,
    undefined: 5,
};
const specialOrderMap = {
    undefined: 1,
    skip: 2,
    reverse: 3,
    drawTwo: 4,
    wild: 5,
    "wild-drawFour": 6,
};
export const sortCards = (cardsArray) => {
    const sorted = cardsArray.slice();

    sorted.sort((cardValue1, cardValue2) => {
        const card1 = cards[cardValue1 - 1];
        const card2 = cards[cardValue2 - 1];
        const colorOrder = colorOrderMap[card1.color] - colorOrderMap[card2.color];
        if (colorOrder === 0) {
            const numberOrder =
                (card1.number != null ? card1.number : 10) -
                (card2.number != null ? card2.number : 10);
            if (numberOrder === 0) {
                return specialOrderMap[card1.special] - specialOrderMap[card2.special];
            }
            return numberOrder;
        }

        return colorOrder;
    });

    return sorted;
};