import React from "react";

const SignUp: React.FC = () => {
    return (
        <form>
            <div className="flex mt-8">
                <span className="lato text-sm text-red-light">*&nbsp;</span>
                <h2 className="lato text-gray-dark text-sm">
                    - поля обязательные для заполнения
                </h2>
            </div>
            <div className="mt-4">
                <label
                    htmlFor="name"
                    className="block text-sm lato text-gray-dark">
                    Имя пользователя
                    <span className="lato text-sm text-red-light">&nbsp;*</span>
                </label>

                <div className="mt-1 rounded-md">
                    <input
                        id="name"
                        className="lato text-sm appearance-none block w-full border border-gray-light rounded-md focus:outline-none focus:shadow-focus-blue focus:border-blue-hover transition duration-150 ease-in-out"
                        style={{padding: '0.5625rem 1rem',}}
                        type="text"
                        name="name"
                    /></div>
            </div>
            <div className="mt-2">
                <label
                    htmlFor="email"
                    className="block text-sm lato text-gray-dark"
                >
                    E-mail
                    <span className="lato text-sm text-red-light">&nbsp;*</span>
                </label>
                <div className="mt-1 rounded-md shadow-sm">
                    <input
                        id="email"
                        className="lato text-sm appearance-none block w-full border border-gray-light rounded-md focus:outline-none focus:shadow-focus-blue focus:border-blue-hover transition duration-150 ease-in-out"
                        style={{padding: '0.5625rem 1rem',}}
                        type="email"
                        name="email"
                    />
                </div>
            </div>
            <div className="mt-2">
                <label
                    htmlFor="password"
                    className="block text-sm lato text-gray-dark"
                >
                    Пароль
                    <span className="lato text-sm text-red-light">&nbsp;*</span>
                </label>
                <div className="mt-1 rounded-md">
                    <input
                        id="password"
                        className="lato text-sm appearance-none block w-full border border-gray-light rounded-md focus:outline-none focus:shadow-focus-blue focus:border-blue-hover transition duration-150 ease-in-out"
                        style={{padding: '0.5625rem 1rem',}}
                        type="password"
                        name="password"
                    />
                </div>
            </div>
            <div className="mt-6">
                <button
                    type="submit"
                    className="w-full flex justify-center btn btnHead text-sm font-bold text-white bg-blue-hover hover:bg-indigo-hover hover:shadow-hover-indigo active:bg-indigo-active active:shadow-active-indigo active:text-gray-light transition duration-150 ease-in-out"
                >
                    Зарегистрироваться
                </button>
            </div>
        </form>
    );
};
export default SignUp;


