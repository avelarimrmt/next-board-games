import {
    actionCardInDiscardTxt,
    actionCardTxt,
    andFurtherTitle,
    descOfGameTitle,
    drawTwoTitle,
    drawTwoTxt,
    exampleTitle,
    exampleTxt,
    goalOfGameTitle,
    howToMoveTitle,
    howToMoveTxt,
    howToWinTitle,
    howToWinTxt,
    ordinaryCardTxt,
    reverseTitle,
    reverseTxt,
    scoringTitle,
    scoringTxt,
    skipTitle,
    skipTxt,
    wildCardTitle,
    wildCardTxt,
    wildCarsTxt,
    wildFourTitle,
    wildFourTxt,
} from "../utils/unoRules";
import React, {useState} from "react";
import {cards} from "../utils/cards";
import styles from "classnames";
import {Card} from "./Card";

const getCardValue = (card) => {
    return cards.findIndex(
        (c) =>
            c.color === card.color &&
            c.number === card.number &&
            c.special === card.special
    ) + 1;
};

export default function Rules({onClick, ...rest}) {
    const redCard = getCardValue({color: "red", number: 4});
    const yellowCard = getCardValue({color: "yellow", number: 9});
    const greenCard = getCardValue({color: "green", number: 1});
    const blueCard = getCardValue({color: "blue", number: 5});

    const skipCard = getCardValue({color: "green", special: "skip"});
    const reverseCard = getCardValue({color: "blue", special: "reverse"});
    const drawTwoCard = getCardValue({color: "yellow", special: "drawTwo"});

    const wildCard = getCardValue({special: "wild"});
    const wildFourCard = getCardValue({special: "wild-drawFour"});

    const [showDesc, setShowDesc] = useState(false);
    const [showHowToMove, setShowHowToMove] = useState(false);
    const [showHowToWin, setShowHowToWin] = useState(false);
    const [showScoring, setShowScoring] = useState(false);
    return (
        <div {...rest}>
            <div className="rules flex flex-col text-black-light leading-desc">
                <div className="flex flex-col mb-4 tablet:pl-10 mobile:pl-6">
                    <p className="mb-1 font-bold tablet:text-base mobile:text-sm text-purple">{goalOfGameTitle}</p>
                    <p className="mt-0.5 lato text-sm pl-4">Первым избавиться от всех карт.</p>
                </div>

                <div className="flex w-full pt-2 pb-2 tablet:pl-10 mobile:pl-6">
                    <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                       onClick={() => (!showDesc ?
                           setShowDesc(true) :
                           setShowDesc(false))}>
                        {descOfGameTitle}<img className="ml-2" src={`${!showDesc ? "/down.svg" : "/up.svg"}`} alt=""/>
                    </a>
                </div>

                <div className="flex w-full px-14 py-6"
                     style={{display: (showDesc ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                    <div className="flex">
                        <p className="lato tablet:text-sm mobile:text-xs ">
                            {ordinaryCardTxt}
                        </p>
                    </div>

                    <div className="flex mt-4 justify-center">
                        <div className="flex mr-5">
                            <Card sizeSM={16} sizeMD={23} card={redCard}/>
                        </div>
                        <div className="flex mr-5">
                            <Card sizeSM={16} sizeMD={23} card={yellowCard}/>
                        </div>
                        <div className="flex mr-5">
                            <Card sizeSM={16} sizeMD={23} card={greenCard}/>
                        </div>
                        <div className="flex">
                            <Card sizeSM={16} sizeMD={23} card={blueCard}/>
                        </div>
                    </div>

                    <div className="flex mt-8">
                        <p className="lato tablet:text-sm mobile:text-xs">{actionCardTxt}</p>
                    </div>

                    <div className="flex mt-3">
                        <p className="lato tablet:text-sm mobile:text-xs">
                            {actionCardInDiscardTxt}
                        </p>
                    </div>

                    <div className="flex mt-4">
                        <div className="flex flex-col">
                            <div className="flex flex-row mb-6">
                                <div className="flex">
                                    <Card sizeSM={16} sizeMD={23} card={drawTwoCard}/>
                                </div>
                                <div className="flex w-53 items-center ml-8">
                                    <p className="lato tablet:text-sm mobile:text-xs">
                                        <span className="font-semibold tablet:text-base mobile:text-sm">{drawTwoTitle}</span><br/>
                                        {drawTwoTxt}
                                    </p>
                                </div>
                            </div>

                            <div className="flex flex-row mb-6">
                                <div className="flex">
                                    <Card sizeSM={16} sizeMD={23} card={reverseCard}/>
                                </div>
                                <div className="flex w-53 items-center ml-8">
                                    <p className="lato tablet:text-sm mobile:text-xs">
                                        <span className="font-semibold tablet:text-base mobile:text-sm">{reverseTitle}</span><br/>
                                        {reverseTxt}
                                    </p>
                                </div>
                            </div>

                            <div className="flex flex-row">
                                <div className="flex">
                                    <Card sizeSM={16} sizeMD={23} card={skipCard}/>
                                </div>
                                <div className="flex w-53 items-center ml-8">
                                    <p className="lato tablet:text-sm mobile:text-xs">
                                        <span className="font-semibold tablet:text-base mobile:text-sm">{skipTitle}</span><br/>
                                        {skipTxt}
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div className="flex mt-8">
                        <p className="lato tablet:text-sm mobile:text-xs">
                            {wildCarsTxt}
                        </p>
                    </div>

                    <div className="flex mt-4">
                        <div className="flex flex-col">
                            <div className="flex flex-row mb-6">
                                <div className="flex">
                                    <Card sizeSM={16} sizeMD={23} card={wildCard}/>
                                </div>
                                <div className="flex w-48 items-center ml-8">
                                    <p className="lato tablet:text-sm mobile:text-xs">
                                        <span className="font-semibold tablet:text-base mobile:text-sm">{wildCardTitle}</span><br/>
                                        {wildCardTxt}
                                    </p>
                                </div>
                            </div>

                            <div className="flex flex-row">
                                <div className="flex">
                                    <Card sizeSM={16} sizeMD={23} card={wildFourCard}/>
                                </div>
                                <div className="flex tablet:w-35 mobile:w-48 items-center ml-8">
                                    <p className="lato tablet:text-sm mobile:text-xs">
                                        <span className="font-semibold tablet:text-base mobile:text-sm">{wildFourTitle}</span><br/>
                                        {wildFourTxt}
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6">
                    <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                       onClick={() => (!showHowToMove ?
                           setShowHowToMove(true) :
                           setShowHowToMove(false))}>
                        {howToMoveTitle}<img className="ml-2" src={`${!showHowToMove ? "/down.svg" : "/up.svg"}`}
                                             alt=""/>
                    </a>
                </div>

                <div className="flex w-full px-14 py-6"
                     style={{display: (showHowToMove ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                    <div className="flex">
                        <p className="lato tablet:text-sm mobile:text-xs">
                            {howToMoveTxt}
                        </p>
                    </div>
                    <div className="flex mt-3">
                        <p className="lato tablet:text-sm mobile:text-xs"><span
                            className="font-bold tablet:text-base mobile:text-sm">{exampleTitle} &nbsp;</span>
                            {exampleTxt}</p>
                    </div>
                </div>

                <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6">
                    <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                       onClick={() => (!showHowToWin ?
                           setShowHowToWin(true) :
                           setShowHowToWin(false))}>
                        {howToWinTitle}<img className="ml-2" src={`${!showHowToWin ? "/down.svg" : "/up.svg"}`} alt=""/>
                    </a>
                </div>

                <div className="flex w-full px-14 py-6"
                     style={{display: (showHowToWin ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                    <div className="flex">
                        <p className="lato tablet:text-sm mobile:text-xs">
                            {howToWinTxt}
                        </p>
                    </div>
                </div>

                <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6">
                    <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                       onClick={() => (!showScoring ?
                           setShowScoring(true) :
                           setShowScoring(false))}>
                        {scoringTitle}<img className="ml-2" src={`${!showScoring ? "/down.svg" : "/up.svg"}`} alt=""/>
                    </a>
                </div>

                <div className="flex w-full px-14 py-6"
                     style={{display: (showScoring ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                    <div className="flex">
                        <p className="lato tablet:text-sm mobile:text-xs">
                            {scoringTxt}
                        </p>
                    </div>

                    <div className="flex mt-4 justify-center">
                        <table className="table">
                            <tbody className="lato tablet:text-sm mobile:text-xs">
                            <tr>
                                <td>Все карты с цифрами (0-9)</td>
                                <td>Подсчет по цифрам на картах</td>
                            </tr>
                            <tr>
                                <td>Активные карты</td>
                                <td>20 баллов</td>
                            </tr>
                            <tr>
                                <td>Черные активные карты</td>
                                <td>50 баллов</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div className="flex flex-col tablet:pl-10 mobile:pl-6 tablet:pr-14 mobile:pr-10 pb-6 pt-4">
                    <p className="mb-1 font-bold tablet:text-base mobile:text-sm text-purple">{andFurtherTitle}</p>
                    <p className="mt-0.5 withIcon clear-left lato tablet:text-sm mobile:text-xs pl-4">
                        Для каждой игры доступна настройка правил. Посмотреть такие правила настроены для следующей
                        партии
                        можно в <a className="linkInRules" onClick={onClick}>Правилах комнаты</a>. Настраивает эти
                        правила игрок, первый зашедшим в комнату – он отмечен {" "}
                    </p>
                </div>
            </div>
            <div className="footerModal flex w-full justify-end">
                <button
                    onClick={rest.onRequestClose}
                    className={styles([
                        "text-white font-bold tablet:text-sm mobile:text-xs leading-btn bg-cyan btnUnderstandSave rounded-def",
                        "focus:outline-none focus:bg-cyan-active focus:text-gray-light focus:shadow-cyan",
                        "hover:bg-cyan-hover hover:shadow-cyan",
                        "duration-150 ease-in-out transition",
                    ])}
                >
                    Понятно
                </button>
            </div>

            <style jsx>{`
            .withIcon:after {
            background-image: url('/uno/gameSession/settings.svg');
            background-size: 100% 100%;
            width: 18px;
            height: 18px;
            display: inline-flex;
            align-items: center;
            padding-top: 10px;
            content: "";
            }
            
            table {
                border: 2px solid #5D2671;
                border-radius: 0.625rem;
                display: block;
            }
            
            td {
                padding: 0.3125rem 1rem;
            }
            
            tr {
                border-bottom: 2px solid #5D2671;
            }
            
            tr:nth-last-child(1) {
                border-bottom: none;
            }
            
            td:nth-child(odd) {
                border-right: 2px solid #5D2671;
            }
            `}
            </style>
        </div>
    );
}
