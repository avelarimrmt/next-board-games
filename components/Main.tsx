import styles from "classnames";
import React from "react";

const MAIN_COLORS = {
    purple: "bgGame",
};
const JUSTIFY = {
    start: "",
    center: "justify-center",
};

export default function Main({ children, color = "purple", justify = "start" }) {
    const className = styles([
        "flex flex-col h-screen relative",
        MAIN_COLORS[color],
        JUSTIFY[justify],
    ]);

    return <main className={className}>{children}</main>;
}
