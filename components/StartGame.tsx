import Heading from "./Heading";
import GameInProgress from "./GameInProgress";
import React from "react";

export default function StartGame({
                                      room,
                                      roomId,
                                      admin,
                                      playersActive,
                                      playersWithCards,
                                      userPlayers,
                                      playerId,
                                  }) {
    if (!playersActive || playersActive.length === 0) {
        return (
            <Heading color="white">
                Loading
            </Heading>
        );
    } else {
        const winner = playersActive.find(
            (player) => (
                (player.data().cards.length === 0 && player.data().isDrop === false)
                || (player.data().cards.length !== 0 && player.data().isDrop === false && playersWithCards.length === 1))
        );

        return (
            <GameInProgress
                room={room}
                roomId={roomId}
                admin={admin}
                playersActive={playersActive}
                playersWithCards={playersWithCards}
                userPlayers={userPlayers}
                playerId={playerId}
                winner={winner}
            />
        );
    }
}