import React from "react";
import ReactModal from "react-modal";
import styles from "classnames";

ReactModal.setAppElement("#__next");

export default function Modal({ children, subtitle, title, ...rest }) {
    return (
        <ReactModal {...rest}>
            <div className="relative bg-white p-10 shadow mobile:flex mobile:flex-col mobile:justify-center">
                <div className="flex items-center justify-between">
                    <div className="flex tablet:justify-start mobile:justify-center w-full">
                        <h1 className="tablet:text-3xl mobile:text-2xl font-extrabold mobile:text-black-light tablet:text-blue">
                            {title}
                        </h1>
                    </div>
                    <button
                        onClick={rest.onRequestClose}
                        className={styles([
                            "tablet:block mobile:hidden focus:outline-none focus:shadow-outline",
                            "duration-150 ease-in-out transition",
                        ])}
                    >
                        <svg id="btnClose" width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.5833 1.41675L1.41666 15.5834M15.5833 15.5834L1.41666 1.41675L15.5833 15.5834Z" stroke="#252525" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                        </svg>

                    </button>
                    <button onClick={rest.onRequestClose}
                                   className={styles([
                                       "absolute tablet:hidden mobile:block focus:outline-none focus:shadow-outline",
                                       "duration-150 ease-in-out transition",
                                   ])}
                            style={{top: '40px', left: '20px',}}
                    >
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M21.284 10.7151H6.03678L10.7035 5.10993C10.9217 4.84739 11.0267 4.50892 10.9954 4.16897C10.964 3.82902 10.7989 3.51545 10.5364 3.29724C10.2738 3.07902 9.93536 2.97404 9.59542 3.00538C9.25547 3.03672 8.9419 3.20182 8.72368 3.46436L2.29569 11.178C2.25244 11.2393 2.21377 11.3038 2.17998 11.3708C2.17998 11.4351 2.17998 11.4736 2.08999 11.5379C2.03172 11.6853 2.00121 11.8422 2 12.0007C2.00121 12.1592 2.03172 12.3162 2.08999 12.4636C2.08999 12.5278 2.08999 12.5664 2.17998 12.6307C2.21377 12.6977 2.25244 12.7622 2.29569 12.8235L8.72368 20.5371C8.84456 20.6822 8.99592 20.799 9.16702 20.8789C9.33811 20.9589 9.52473 21.0002 9.71359 20.9999C10.014 21.0005 10.3051 20.8959 10.5364 20.7042C10.6666 20.5963 10.7742 20.4638 10.853 20.3142C10.9319 20.1646 10.9805 20.001 10.996 19.8326C11.0116 19.6642 10.9937 19.4944 10.9435 19.3329C10.8933 19.1714 10.8118 19.0215 10.7035 18.8916L6.03678 13.2863H21.284C21.6249 13.2863 21.9519 13.1509 22.193 12.9098C22.4341 12.6687 22.5696 12.3417 22.5696 12.0007C22.5696 11.6598 22.4341 11.3328 22.193 11.0917C21.9519 10.8506 21.6249 10.7151 21.284 10.7151Z" fill="black"/>
                        </svg>
                    </button>
                </div>
                <div className="flex items-center justify-between">
                    <div className="flex tablet:justify-start mobile:justify-center w-full">
                        <h2 className="block lato tablet:text-base mobile:text-sm tablet:text-black-light mobile:text-gray-dark">
                            {subtitle}
                        </h2>
                    </div>
                </div>
                {children}
            </div>
            <style jsx>{`
            #btnClose:hover path {
                stroke: #3F5FCE;
            }
            
            #btnClose:active path {
                stroke: #2D4BB2;
            }
            `}</style>
        </ReactModal>
    );
}