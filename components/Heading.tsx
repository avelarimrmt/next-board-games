import React from "react";
import styles from "classnames";

const HEADER_STYLES = {
    h1: ["text-xl", "md:text-2xl"],
    h2: ["text-lg", "md:text-xl"],
    h3: [],
    h4: [],
    h5: [],
    h6: [],
};
const HEADER_COLORS = {
    white: "text-white",
    black: "text-black",
};

export default function Heading({
                                    children,
                                    textCenter = true,
                                    color = "black",
                                    margin = "0",
                                    marginBottom = "0",
                                    className = "",
                                }) {

    className = styles([
        "font-medium",
        textCenter ? "text-center" : null,
        ...HEADER_STYLES["h1"],
        HEADER_COLORS[color],
        `m-${margin}`,
        `mb-${marginBottom}`, {className}
    ]);

    return <h1 className={className}>{children}</h1>;
}
