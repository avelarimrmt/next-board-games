import React from "react";

export default function WildCardOptions({ onChooseColor }) {
    const vw = window.innerWidth;
    return (
        <div className="flex tablet:flex-col mobile:flex-row tablet:mt-0 mobile:mt-10 pl-4 pr-4 justify-center">
            <button
                type="button"
                className="color tablet:mr-0 mobile:mr-2 mb-2 bg-red hover:bg-red-hover text-white font-bold text-sm rounded-def
                focus:outline-none focus:bg-red-active focus:text-gray-light"
                onClick={() => onChooseColor("red")}
            >
                {vw >= 768 ? "Красный" : null}
            </button>
            <button
                type="button"
                className="color tablet:mr-0 mobile:mr-2 mb-2 bg-yellow hover:bg-yellow-hover text-white font-bold text-sm rounded-def
                focus:outline-none focus:bg-yellow-active focus:text-gray-light"
                onClick={() => onChooseColor("yellow")}
            >
                {vw >= 768 ? "Желтый" : null}
            </button>
            <button
                type="button"
                className="color tablet:mr-0 mobile:mr-2 mb-2 bg-cyan hover:bg-cyan-hover text-white font-bold text-sm rounded-def
                focus:outline-none focus:bg-cyan-active focus:text-gray-light"
                onClick={() => onChooseColor("green")}
            >
                {vw >= 768 ? "Зеленый" : null}
            </button>
            <button
                type="button"
                className="color bg-indigo hover:bg-indigo-hover text-white font-bold text-sm rounded-def
                focus:outline-none active:bg-indigo-active active:text-gray-light"
                onClick={() => onChooseColor("blue")}
            >
                {vw >= 768 ? "Синий" : null}
            </button>

        </div>
    );
}
