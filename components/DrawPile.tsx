import {BackCard} from "./Card";
import React from "react";

export default function DrawPile({
                                     onDrawCard,
                                     canDrawFromPile,
                                     isCurrentPlayerTurn,
                                     drawPileRef,
                                 }) {
    return (
        <button
            onClick={(e) => onDrawCard()}
            disabled={!(canDrawFromPile && isCurrentPlayerTurn)}
            className={`tablet:mr-10 tablet:mt-0 mobile:mr-2 mobile:mt-1 btnDrawPile ${(canDrawFromPile && isCurrentPlayerTurn) ? "shadowPile" : null}`}
        >
            <div
                className="h-full drawPile"
            >
                <div style={{
                    top: 0,
                    position: "absolute",
                    left: 0,
                }}
                className="tablet:mt-0 mobile:-mt-2">
                    <BackCard sizeSM={16} sizeMD={"discard"}/>
                </div>
                <div
                    style={{
                        top: 0,
                        position: "absolute",
                        left: ".5em",
                    }}
                    className="tablet:mt-0 mobile:-mt-2">
                    <BackCard sizeSM={16} sizeMD={"discard"}/>
                </div>
                <div
                    style={{
                        top: 0,
                        position: "absolute",
                        left: "1em",
                    }}
                    ref={drawPileRef}
                    className="tablet:mt-0 mobile:-mt-2">
                    <BackCard  sizeSM={16} sizeMD={"discard"}/>
                </div>
            </div>
            <style jsx>{`
                @media (min-width: 320px) {
                    .drawPile {
                    width: 79px;
                    height: 84px;
                    }
                }
            
                @media (min-width: 768px) {
                    .drawPile {
                    width: 99px;
                    height: 130px;
                    margin-left: -2px;
                    margin-right: 2px;
                    }
                }
                
                .drawPile {
                    position: relative;
                }
                
            `}</style>
        </button>
    );
}