import styles from "classnames";
import React from "react";

const BUTTON_COLORS = {
    green: [
        "bg-cyan",
        "text-white",
        "hover:text-white",
    ],
    red: ["bg-red-500", "focus:bg-red-500", "hover:bg-red-700", "text-white"],
    yellow: ["bg-yellow-300", "focus:bg-yellow-500", "text-yellow-800"],
    gray: ["bg-gray-300", "focus:bg-gray-500", "text-white"],
};

export default function Button({
                                   ariaLabel,
                                   children,
                                   className = "font-medium rounded-def text-center",
                                   color = "yellow",
                                   disabled,
                                   onClick,
                                   onBlur,
                               }) {
    return (
        <>
            <button
                type="button"
                aria-label={ariaLabel}
                className={styles([

                    "focus:outline-none focus:shadow-outline",
                    "duration-150 ease-in-out transition",
                    (className),

                    [...BUTTON_COLORS[color]],
                ])}
                disabled={disabled}
                onClick={onClick}
                onBlur={onBlur}
            >
                <span className="flex items-center justify-center w-full">
                {children}
                </span>
            </button>
        </>
    );
}
