import {Card, BackCard} from "./Card";
import React from "react";

export default function PlayerCards({
                                        cards,
                                        isCurrentPlayer,
                                        isCardDisabled,
                                        onDiscardACard,
                                        onCardAdd,
                                        onCardRemove,
                                        winner,
                                        hintsWhatCards
                                    }) {
    return (
        <div
            className={
                isCurrentPlayer
                    ? `tablet:z-10 mobile:z-0 tablet:relative mobile:absolute mobile:bottom-17 tablet:bottom-0 flex flex-grow items-end justify-center w-full`
                    : "w-full"
            }
        >
            <div
                className={`w-full flex flex-row flex-no-wrap justify-center ${
                    isCurrentPlayer ? "tablet:h-player mobile:h-playerM flex items-start overflow-hidden overflow-x-clip" : "flex-auto items-center pb-1 tablet:h-32 mobile:h-12 relative"
                }`}
                style={{maxWidth: '98vw',}}
            >
                {cards.map((card, index) => {
                    const disabled = isCardDisabled(card);

                    return isCurrentPlayer ? (
                        <div
                            key={card}
                            className="mobile:-mx-5 tablet:-mx-8 flex flex-col justify-center"
                        >
                            <button onClick={() => onDiscardACard(card)} disabled={disabled}>
                                <Card
                                    onRemove={onCardRemove}
                                    onAdd={onCardAdd}
                                    sizeSM={16}
                                    sizeMD={"player"}
                                    card={card}
                                    opacity={hintsWhatCards ? (disabled ? "opacity-50" : "opacity-100") : null}
                                />
                            </button>
                        </div>
                    ) : (
                        <div
                            key={card + `_Back`}
                            className="absolute"
                            style={{
                                left: `${(50 / (cards.length / 2 + 4)) * (index + 4)}%`,
                            }}
                        >
                            { (
                                <BackCard
                                    onRemove={onCardRemove}
                                    onAdd={onCardAdd}
                                    sizeSM={2.5}
                                    sizeMD={"back"}
                                />
                            )}
                        </div>
                    );
                })}
            </div>
        </div>
    );
}