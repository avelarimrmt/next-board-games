import React from "react";

export default function CurrentMovePlayerOptions({
                                                      currentMovePlayer,
                                                      playerId,
                                                      playersActive,
                                                      onPassTurn,
                                                      room,
                                                      onYellOne,
                                                      display = (room.drawPile === false ? "hidden" : "block")
                                                  }) {
    return (
        <div className={`flex justify-center ${
                    display === "hidden" ? "h-0" : ""
                }`}>
            <div
                className={`flex justify-center ${
                    currentMovePlayer.id === playerId ? "block" : "hidden"
                }`}
            >
                <button
                    onClick={() => onPassTurn(room.currentMove)}
                    className={`passTurn tablet:text-sm mobile:text-xs text-cyan border border-cyan rounded-def font-bold ${
                        display
                    } focus:outline-none hover:bg-cyan-hover hover:bg-opacity-15 hover:border-cyan-hover hover:text-cyan-hover
                focus:bg-cyan-active focus:bg-opacity-15 focus:border-cyan-active focus:text-cyan-active`}
                    disabled={room.drawPile === false}
                >
                    Пропустить
                </button>
            </div>
            <div className="absolute tablet:bottom-10 mobile:bottom-2.5 mobile:right-5 tablet:w-full mobile:w-auto tablet:right-96">
                <div className="uno flex justify-end">
                    <button
                        onClick={() => onYellOne(room.currentMove)}
                        className={`z-20 btnUno text-white font-black tablet:text-4xl mobile:text-mxl ${currentMovePlayer && currentMovePlayer.id === playerId && currentMovePlayer.data().cards.length == 2 && room.hintsForBtn ? "hintsForBtn" : null}`}
                        disabled={currentMovePlayer && currentMovePlayer.id !== playerId}
                    >
                        UNO
                    </button>
                </div>
            </div>
        </div>
    );
}