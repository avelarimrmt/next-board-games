import React from "react";
import ReactModal from "react-modal";
import {cards} from "../utils/cards";
import db from "../config/firebase";

ReactModal.setAppElement("#__next");

export default function ModalWinner({room, players, winner, winners, children, buttons, ...rest}) {
    const score = [];
    const playersSlots = [];
    const playersLose = [];
    const playersDrop = [];
    const playerInfo = [];

    if (!room.unoToTheLast) {
        for (let i = 0; i < players.length; i++) {
            if (players[i] !== winner) {
                const player = players[i];
                const countCards = player.data().cards.length;

                if (countCards != 0)
                    playersLose.push(
                        <td className="text-base w-lose">
                            {player ? player.data().name : null}
                        </td>
                    );
                else playersDrop.push(
                    <td className="text-base w-lose">
                        {player ? player.data().name : null}
                    </td>);

                let sum = 0;

                for (let j = 0; j < countCards; j++) {
                    const number = player.data().cards[j];
                    sum += cards[number - 1].score;
                }
                if (countCards != 0) score.push(sum);
            }
        }
        let sortByScore = (a, b) => a.score - b.score;
        for (let i = 0; i < playersLose.length; i++) {
            playerInfo[i] = {
                name: playersLose[i],
                score: score[i]
            };
        }
        playerInfo.sort(sortByScore);
        for (let i = 0; i < playersLose.length; i++) {
            playersSlots.push(
                <tr className="strokePlayer w-full leading-8">
                    <td className="pt-3.5 pb-3.5 pl-16 text-xl w-number">{i + 2}</td>
                    {playerInfo[i].name}
                    <td className={`pt-3.5 pb-3.5 text-right text-xl text-purple pr-16 w-sum `}>{playerInfo[i].score}</td>
                </tr>
            );
        }
        if (playersDrop) {
            for (let i = 0; i < playersDrop.length; i++) {
                playersSlots.push(
                    <tr className="strokePlayer w-full leading-8">
                        <td className="pt-3.5 pb-3.5 pl-16 text-xl w-number">{playersLose.length + i + 2}</td>
                        {playersDrop[i]}
                        <td className={`pt-3.5 pb-3.5 text-left text-gray-dark font-medium text-sm w-sum `}>
                            Игрок сбросил карты
                        </td>
                    </tr>
                );
            }
        }
    } else {
        for (let i = 1; i < winners.length; i++) {
            const playerWin = winners[i];
            playersLose.push(
                <td className="text-base w-lose">
                    {playerWin ? playerWin.name : null}
                </td>);
        }

        for (let i = 0; i < players.length; i++)
            if (players[i].id !== winner) {
                const player = players[i];
                if (player.data().isDrop)
                    playersDrop.push(
                        <td className="text-base w-lose">
                            {player ? player.data().name : null}
                        </td>);
            }

        for (let i = 0; i < players.length; i++) {
            if (players[i].id !== winner) {
                const player = players[i];
                if (player.data().cards.length !== 0)
                    if (winners.length !== 0)
                    playersLose.push(
                        <td className="text-base w-lose">
                            {player ? player.data().name : null}
                        </td>);
            }
        }

        for (let i = 0; i < playersLose.length; i++) {
            playersSlots.push(
                <tr className="strokePlayer w-full leading-8">
                    <td className="pt-3.5 pb-3.5 pl-16 text-xl w-number">{i + 2}</td>
                    {playersLose[i]}
                    <td className="pt-3.5 pb-3.5 pl-16 w-sum"></td>
                </tr>
            );
        }
        if (playersDrop)
            for (let i = 0; i < playersDrop.length; i++) {
                playersSlots.push(
                    <tr className="strokePlayer w-full leading-8">
                        <td className="pt-3.5 pb-3.5 pl-16 text-xl w-number">{playersLose.length + i + 2}</td>
                        {playersDrop[i]}
                        <td className={`pt-3.5 pb-3.5 text-left text-gray-dark font-medium text-sm w-sum `}>
                            Игрок сбросил карты
                        </td>
                    </tr>
                );
            }
    }
    return (
        <ReactModal {...rest}>
            <div className="bg-blue-dark shadow">
                <div className="flex w-full bgGame justify-center" style={{height: '19.375rem'}}>
                    <div className="flex bgFireworks h-full w-full">
                        <div className="flex flex-col w-full h-full items-center">
                            <h1 className="font-extrabold text-white tablet:text-4xl mobile:text-xl" style={{marginTop: '2.375rem'}}>
                                Игра окончена</h1>

                            <div className="flex flex-col items-center">
                                <h1 className="font-black text-yellow text-over mb-2">1</h1>
                                <div className="flex flex-row justify-around" style={{width: '25.5rem'}}>
                                    <div className="left"><img src="/uno/gameSession/leftLaurel.svg" alt=""/></div>
                                    <div className="player h-full flex flex-col justify-center items-center">
                                        <div
                                            className={`relative avatar avatar${!room.unoToTheLast ? (winner ? winner.data().avatar : null) 
                                                : (winners[0] ? winners[0].avatar : (winner ? winner.data().avatar : null) )
                                                } rounded-2.5xl border-5 w-winner h-winner mb-2.5`}>
                                        </div>

                                        <div
                                            className="nick rounded-lg flex items-center justify-center">
                                            {winner ?
                                                <p className="truncate font-bold text-sm text-white py-0 px-4">{!room.unoToTheLast ? (winner ? winner.data().name : null)
                                                    : (winners[0] ? winners[0].name : (winner ? winner.data().name : null))
                                                   }</p> : null}
                                        </div>
                                    </div>
                                    <div className="right"><img src="/uno/gameSession/rightLaurel.svg" alt=""/></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="flex flex-grow bg-white pt-6">
                    <div className="players w-full flex">
                        <table className="table w-full lato font-bold text-black-light">
                            <tbody className="">
                            {playersSlots}
                            </tbody>
                        </table>
                        {children}
                    </div>
                </div>
                <div className="footerModal flex w-full justify-center">
                    {buttons}
                </div>
            </div>
        </ReactModal>
    );
}