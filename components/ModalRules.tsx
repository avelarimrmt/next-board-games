import React from "react";
import ReactModal from "react-modal";

ReactModal.setAppElement("#__next");

export default function ModalRules({ children, title, isAdmin = null, ...rest }) {
    return (
        <ReactModal {...rest}>
            <div className="bg-white shadow">
                <div className="flex items-center justify-between">
                    <div className="flex justify-start w-full">
                        <h1 className={`pt-10 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6 ${title == "Правила игры UNO" ? "pb-10" : null} ${isAdmin ? "pb-10" : null} tablet:text-2xl mobile:text-mxl font-extrabold text-black-light`}>
                            {title}
                        </h1>
                    </div>
                </div>
                {children}
            </div>
        </ReactModal>
    );
}