import { Card } from "./Card";
import React from "react";

export default function DiscardPile({ discardPile, discardColor, pileRef }) {
    return (
        <div ref={pileRef}>
            <Card
                sizeSM={16}
                sizeMD={"discard"}
                card={discardPile}
                wildColor={discardColor}
            />
        </div>
    );
}
