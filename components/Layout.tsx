import Header from "./Header";
import React from "react";

export default function Layout({children,
                                   size = null,
                                   admin = null,
                                   playersActive = null,
                                   userPlayers = null,
                                   playersWithCards = null,
                                   room, roomRef = null})
{
    return (
        <>
            <Header size={size}
            admin={admin}
            playersActive={playersActive}
            userPlayers={userPlayers}
            playersWithCards={playersWithCards}
            room={room}
            roomRef={roomRef}/>
            {children}
        </>
    );
}