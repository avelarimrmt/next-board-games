import React from "react";
import styles from "classnames";

const CONTAINER_SIZES = {
    small: "md:w-2/5",
    large: "w-full",
};

export default function Container({children, size = "small"}) {
    return (
        <div className="max-w-6xl mx-auto">
            <div className={styles(["mx-auto", CONTAINER_SIZES[size]])}>
                {children}
            </div>
        </div>
    );
}