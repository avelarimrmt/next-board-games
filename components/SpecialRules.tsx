import {
    accumTitle,
    accumTxt,
    hintsWhatCardsCanUseTitle, hintsWhatCardsCanUseTxt,
    interventionTitle,
    interventionTxt,
    numberOfCards,
    tipsForBtnUnoTitle,
    tipsForBtnUnoTxt,
    twoSameCardsTitle, twoSameCardsTxtBlack, unoLastTitle, unoLastTxt, wildFourDifficultyTitle,
    wildFourDifficultyTxt
} from "../utils/unoSpecialRules";
import React, {useState} from "react";
import styles from "classnames";
import db from "../config/firebase";


export default function SpecialRules({admin, room, roomRef, roomId, ...rest}) {
    const [showHintsWhatCards, setShowHintsWhatCards] = useState(false);
    const [showTipsBtn, setShowTipsBtn] = useState(false);
    const [showWildFourDiff, setShowWildFourDiff] = useState(false);
    const [showAccum, setShowAccum] = useState(false);
    const [showUnoLast, setShowUnoLast] = useState(false);
    /*const [showIntervention, setShowIntervention] = useState(false);*/
    const [showTwoSameCards, setShowTwoSameCards] = useState(false);
    const [hintsWhatCards, setHintsWhatCards] = useState(roomRef ? roomRef.hintsWhatCards : room.hintsWhatCards);
    const [hintsForBtn, setHintsForBtn] = useState(roomRef ? roomRef.hintsForBtn : room.hintsForBtn);
    const [wildFourDifficulty, setWildFourDifficulty] = useState(roomRef ? roomRef.wildFourDifficulty : room.wildFourDifficulty);
    const [unoToTheLast, setUnoToTheLast] = useState(roomRef ? roomRef.unoToTheLast : room.unoToTheLast);
    const [dropSameCards, setDropSameCards] = useState(roomRef ? roomRef.dropSameCards : room.dropSameCards);
    const [accum, setAccum] = useState(roomRef ? roomRef.accum : room.accum);

    const [number, setNumber] = useState(roomRef ? roomRef.number : room.number);

    const minValueCards = 5;
    const maxValueCards = 14;

    const plusNumber = () => {
        if (number !== maxValueCards && number >= minValueCards) {
            setNumber(number + 1);
        }
    };

    const minusNumber = () => {
        if (number !== minValueCards && number <= maxValueCards) {
            setNumber(number - 1);
        }
    };


    const handleSubmit = (event) => {
        event.preventDefault();

        if (roomId) {
            const roomRef = db.collection("rooms").doc(roomId.toString());
            roomRef.update({
                number: number,
                hintsWhatCards: hintsWhatCards,
                hintsForBtn: hintsForBtn,
                wildFourDifficulty: wildFourDifficulty,
                unoToTheLast: unoToTheLast,
                dropSameCards: dropSameCards,
                accum: accum,
            });
        }
    };

    const handleReset = (event) => {
        event.preventDefault();
        if (roomId) {
            const roomRef = db.collection("rooms").doc(roomId.toString());
            roomRef.update({
                number: 7,
                hintsWhatCards: true,
                hintsForBtn: false,
                wildFourDifficulty: false,
                unoToTheLast: false,
                dropSameCards: false,
                accum: false,
            });
        }
        setNumber(7);
        setHintsWhatCards(true);
        setHintsForBtn(false);
        setWildFourDifficulty(false);
        setUnoToTheLast(false);
        setDropSameCards(false);
        setAccum(false);
    };

    if (admin) {
        return (
            <div {...rest}>
                <form onSubmit={handleSubmit}>
                    <div className="rules flex flex-col text-black-light leading-desc">
                        <div className="flex flex-row w-full pb-2 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6 items-center justify-between">
                            <p className="flex w-full items-center font-bold tablet:text-base mobile:text-sm">
                                {numberOfCards}
                            </p>
                            <div className="flex">
                                <button onClick={minusNumber} className="minus" type="button">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M10.0938 0.59375C4.8555 0.59375 0.59375 4.8555 0.59375 10.0938C0.59375 15.332 4.8555 19.5938 10.0938 19.5938C15.332 19.5938 19.5938 15.332 19.5938 10.0938C19.5938 4.8555 15.332 0.59375 10.0938 0.59375ZM14.4784 10.8245H10.8245H9.36298H5.70913V9.36298H9.36298H10.8245H14.4784V10.8245Z"
                                            fill="#27DC95"/>
                                    </svg>
                                </button>
                                <input type="text" disabled
                                       className="w-8 bg-transparent text-center tablet:text-lg mobile:text-sm font-medium"
                                       value={number}
                                       onChange={(e) => setNumber(Number.parseInt(e.target.value))}/>
                                <button onClick={plusNumber} className="minus" type="button">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M10.0938 0.59375C4.8555 0.59375 0.59375 4.8555 0.59375 10.0938C0.59375 15.332 4.8555 19.5938 10.0938 19.5938C15.332 19.5938 19.5938 15.332 19.5938 10.0938C19.5938 4.8555 15.332 0.59375 10.0938 0.59375ZM14.4784 10.8245H10.8245V14.4784H9.36298V10.8245H5.70913V9.36298H9.36298V5.70913H10.8245V9.36298H14.4784V10.8245Z"
                                            fill="#27DC95"/>
                                    </svg>
                                </button>
                            </div>

                        </div>

                        <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                            <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                               onClick={() => (!showHintsWhatCards ?
                                   setShowHintsWhatCards(true) :
                                   setShowHintsWhatCards(false))}>
                                {hintsWhatCardsCanUseTitle}<img className="ml-2"
                                                                src={`${!showHintsWhatCards ? "/down.svg" : "/up.svg"}`}
                                                                alt=""/>
                            </a>
                            <input type="checkbox" checked={hintsWhatCards}
                                   className="form-checkbox border-2 border-black-light text-cyan h-4"
                                   onChange={(e) => setHintsWhatCards(e.target.checked)}
                            />
                        </div>

                        <div className="flex w-full px-14 py-6 "
                             style={{
                                 display: (showHintsWhatCards ? 'block' : 'none'),
                                 background: 'rgba(93,38,113,0.1)'
                             }}>
                            <div className="flex">
                                <p className="lato tablet:text-sm mobile:text-xs ">
                                    {hintsWhatCardsCanUseTxt}
                                </p>
                            </div>
                        </div>

                        <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6 items-center">
                            <a className="flex w-full  items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                               onClick={() => (!showTipsBtn ?
                                   setShowTipsBtn(true) :
                                   setShowTipsBtn(false))}>
                                {tipsForBtnUnoTitle}<img className="ml-2"
                                                         src={`${!showTipsBtn ? "/down.svg" : "/up.svg"}`}
                                                         alt=""/>
                            </a>
                            <input type="checkbox" checked={hintsForBtn}
                                   className="form-checkbox border-2 border-black-light text-cyan h-4"
                                   onChange={(e) => setHintsForBtn(e.target.checked)}
                            />
                        </div>

                        <div className="flex w-full px-14 py-6 "
                             style={{display: (showTipsBtn ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                            <div className="flex">
                                <p className="lato tablet:text-sm mobile:text-xs">
                                    {tipsForBtnUnoTxt}
                                </p>
                            </div>
                        </div>

                        <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6 items-center">
                            <a className="flex w-full  items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                               onClick={() => (!showUnoLast ?
                                   setShowUnoLast(true) :
                                   setShowUnoLast(false))}>
                                {unoLastTitle}<img className="ml-2"
                                                   src={`${!showUnoLast ? "/down.svg" : "/up.svg"}`}
                                                   alt=""/>
                            </a>
                            <input type="checkbox" checked={unoToTheLast}
                                   className="form-checkbox border-2 border-black-light text-cyan h-4"
                                   onChange={(e) => setUnoToTheLast(e.target.checked)}
                            />
                        </div>

                        <div className="flex w-full px-14 py-6 "
                             style={{display: (showUnoLast ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                            <div className="flex">
                                <p className="lato tablet:text-sm mobile:text-xs">
                                    {unoLastTxt}
                                </p>
                            </div>
                        </div>

                        <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                            <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                               onClick={() => (!showWildFourDiff ?
                                   setShowWildFourDiff(true) :
                                   setShowWildFourDiff(false))}>
                                {wildFourDifficultyTitle}<img className="ml-2"
                                                              src={`${!showWildFourDiff ? "/down.svg" : "/up.svg"}`}
                                                              alt=""/>
                            </a>
                            <input type="checkbox" checked={wildFourDifficulty}
                                   className="form-checkbox border-2 border-black-light text-cyan h-4"
                                   onChange={(e) => setWildFourDifficulty(e.target.checked)}
                            />
                        </div>

                        <div className="flex w-full px-14 py-6"
                             style={{
                                 display: (showWildFourDiff ? 'block' : 'none'),
                                 background: 'rgba(93,38,113,0.1)'
                             }}>
                            <div className="flex">
                                <p className="lato tablet:text-sm mobile:text-xs">
                                    {wildFourDifficultyTxt}
                                </p>
                            </div>
                        </div>

                        <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                            <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                               onClick={() => (!showAccum ?
                                   setShowAccum(true) :
                                   setShowAccum(false))}>
                                {accumTitle}<img className="ml-2" src={`${!showAccum ? "/down.svg" : "/up.svg"}`}
                                                 alt=""/>
                            </a>
                            <input type="checkbox" checked={accum}
                                   className="form-checkbox border-2 border-black-light text-cyan h-4"
                                   onChange={(e) => setAccum(e.target.checked)}
                            />
                        </div>

                        <div className="flex w-full px-14 py-6"
                             style={{display: (showAccum ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                            <div className="flex">
                                <p className="lato tablet:text-sm mobile:text-xs">
                                    {accumTxt}
                                </p>
                            </div>
                        </div>

                        {/*<div className="flex w-full pt-4 pb-2 pl-10 items-center pr-10">
                        <a className="flex w-full items-center cursor-pointer font-bold text-base"
                           onClick={() => (!showIntervention ?
                               setShowIntervention(true) :
                               setShowIntervention(false))}>
                            {interventionTitle}<img className="ml-2"
                                                    src={`${!showIntervention ? "/down.svg" : "/up.svg"}`} alt=""/>
                        </a>
                        <input type="checkbox"
                               className="form-checkbox border-2 border-black-light text-cyan h-4"
                        />
                    </div>

                    <div className="flex w-full px-14 py-6"
                         style={{display: (showIntervention ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                        <div className="flex">
                            <p className="lato text-sm ">
                                {interventionTxt}
                            </p>
                        </div>
                    </div>*/}

                        <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                            <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                               onClick={() => (!showTwoSameCards ?
                                   setShowTwoSameCards(true) :
                                   setShowTwoSameCards(false))}>
                                {twoSameCardsTitle}<img className="ml-2"
                                                        src={`${!showTwoSameCards ? "/down.svg" : "/up.svg"}`} alt=""/>
                            </a>
                            <input type="checkbox" checked={dropSameCards}
                                   className="form-checkbox border-2 border-black-light text-cyan h-4"
                                   onChange={(e) => setDropSameCards(e.target.checked)}
                            />
                        </div>

                        <div className="flex w-full px-14 py-6"
                             style={{
                                 display: (showTwoSameCards ? 'block' : 'none'),
                                 background: 'rgba(93,38,113,0.1)'
                             }}>
                            <div className="flex flex-col">
                                {/*<p className="lato text-sm mb-4 text-gray-dark">
                                {twoSameCardsTxtGray}
                            </p>*/}
                                <p className="lato tablet:text-sm mobile:text-xs">
                                    {twoSameCardsTxtBlack}
                                </p>
                            </div>
                        </div>

                        <div className="flex w-full pb-6">
                        </div>
                    </div>

                    <div className="footerModal flex w-full justify-end">
                        <div className="flex w-full justify-between">
                            <button type="reset" disabled={roomRef ? roomRef.playing : room.playing} onClick={handleReset} className={styles([
                                "flex tablet:flex-row mobile:flex-col rulDefault lato tablet:text-sm mobile:text-xs items-center focus:outline-none",
                            ])}>
                                <svg className="iconInLink" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M12 3C9.61305 3 7.32387 3.94821 5.63604 5.63604C3.94821 7.32387 3 9.61305 3 12H0L4 16L8 12H5C5 8.13 8.13 5 12 5C15.87 5 19 8.13 19 12C19.0014 13.2858 18.6483 14.5471 17.9796 15.6453C17.3109 16.7435 16.3524 17.6362 15.2095 18.2252C14.0666 18.8142 12.7834 19.0769 11.5009 18.9842C10.2185 18.8916 8.98639 18.4472 7.94 17.7L6.52 19.14C7.65464 20.0107 8.98046 20.5985 10.3876 20.8545C11.7947 21.1106 13.2426 21.0277 14.6112 20.6126C15.9799 20.1975 17.2299 19.4622 18.2577 18.4676C19.2855 17.473 20.0614 16.2478 20.5213 14.8935C20.9811 13.5392 21.1115 12.0949 20.9018 10.6801C20.692 9.26534 20.1481 7.92094 19.3151 6.75832C18.4822 5.59569 17.3841 4.64833 16.112 3.99474C14.8398 3.34114 13.4302 3.00015 12 3Z"
                                        fill="#5D2671"/>
                                    <path
                                        d="M14.2922 12.294C14.3045 12.198 14.3138 12.102 14.3138 12C14.3138 11.898 14.3045 11.802 14.2922 11.706L14.943 11.211C15.0016 11.166 15.017 11.085 14.98 11.019L14.3631 9.98101C14.3261 9.91501 14.2429 9.89101 14.175 9.91501L13.4071 10.215C13.2467 10.095 13.074 9.99601 12.8858 9.92101L12.7686 9.12601C12.7635 9.09072 12.7454 9.05844 12.7175 9.03524C12.6897 9.01205 12.6542 8.99952 12.6175 9.00001H11.3838C11.3067 9.00001 11.242 9.05401 11.2327 9.12601L11.1155 9.92101C10.9274 9.99601 10.7547 10.098 10.5943 10.215L9.82635 9.91501C9.75541 9.88801 9.67522 9.91501 9.63821 9.98101L9.02138 11.019C8.98129 11.085 8.99979 11.166 9.05839 11.211L9.70915 11.706C9.69681 11.802 9.68756 11.901 9.68756 12C9.68756 12.099 9.69681 12.198 9.70915 12.294L9.05839 12.789C8.99979 12.834 8.98437 12.915 9.02138 12.981L9.63821 14.019C9.67522 14.085 9.7585 14.109 9.82635 14.085L10.5943 13.785C10.7547 13.905 10.9274 14.004 11.1155 14.079L11.2327 14.874C11.242 14.946 11.3067 15 11.3838 15H12.6175C12.6946 15 12.7594 14.946 12.7686 14.874L12.8858 14.079C13.074 14.004 13.2467 13.902 13.4071 13.785L14.175 14.085C14.2459 14.112 14.3261 14.085 14.3631 14.019L14.98 12.981C15.017 12.915 15.0016 12.834 14.943 12.789L14.2922 12.294ZM12.0007 13.05C11.4054 13.05 10.9212 12.579 10.9212 12C10.9212 11.421 11.4054 10.95 12.0007 10.95C12.5959 10.95 13.0801 11.421 13.0801 12C13.0801 12.579 12.5959 13.05 12.0007 13.05Z"
                                        fill="#5D2671"/>
                                </svg>
                                &nbsp;
                                <span className="linkInRules">Правила по умолчанию</span>
                            </button>
                            <div className="flex items-center">
                                <button
                                    onClick={rest.onRequestClose}
                                    className={styles([
                                        "tablet:text-sm mobile:text-xs leading-btn font-bold text-black-light",
                                        "border border-black-light rounded-def btnCancel",
                                        "hover:bg-gray-light",
                                        "focus:outline-none focus:text-gray-dark",
                                        "duration-150 ease-in-out transition", "tablet:mr-5.5 mobile:mr-2.5"
                                    ])}
                                    type="button"
                                >
                                    Отмена
                                </button>
                                <button
                                    className={styles([
                                        "btnSave text-white font-bold tablet:text-sm mobile:text-xs leading-btn bg-cyan btnUnderstandSave rounded-def",
                                        "focus:outline-none active:bg-cyan-active",
                                        "duration-150 ease-in-out transition",
                                    ])}
                                    type="submit"
                                    disabled={roomRef ? roomRef.playing : room.playing}
                                    onClick={() => {
                                        setTimeout(rest.onRequestClose, 200)
                                    }}

                                >
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    } else
        return (
            <div>
                <div className="flex flex-col text-black-light leading-desc pt-2">
                    <div className="flex w-full tablet:pl-10 mobile:pl-6 pb-10">
                        <p className="lato text-gray-dark text-xs">
                            Здесь отображаются правила, которые установил администратор комнаты
                        </p>
                    </div>

                    <div className="rules flex flex-col text-black-light leading-desc">
                        <div className="flex flex-col w-full pb-6 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                            <p className="flex w-full items-center font-bold tablet:text-base mobile:text-sm">
                                {numberOfCards}
                            </p>
                            <p className="tablet:text-sm mobile:text-xs lato">{roomRef ? roomRef.number : room.number}</p>
                        </div>

                        {
                            (roomRef ? roomRef.hintsWhatCards : room.hintsWhatCards) ?
                                <div>
                                    <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                                        <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                                           onClick={() => (!showHintsWhatCards ?
                                               setShowHintsWhatCards(true) :
                                               setShowHintsWhatCards(false))}>
                                            {hintsWhatCardsCanUseTitle}<img className="ml-2"
                                                                            src={`${!showHintsWhatCards ? "/down.svg" : "/up.svg"}`}
                                                                            alt=""/>
                                        </a>
                                    </div>

                                    <div className="flex w-full px-14 py-6 "
                                         style={{
                                             display: (showHintsWhatCards ? 'block' : 'none'),
                                             background: 'rgba(93,38,113,0.1)'
                                         }}>
                                        <div className="flex">
                                            <p className="lato tablet:text-sm mobile:text-xs">
                                                {hintsWhatCardsCanUseTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }

                        {
                            (roomRef ? roomRef.hintsForBtn : room.hintsForBtn) ?
                                <div>
                                    <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6 items-center">
                                        <a className="flex w-full  items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                                           onClick={() => (!showTipsBtn ?
                                               setShowTipsBtn(true) :
                                               setShowTipsBtn(false))}>
                                            {tipsForBtnUnoTitle}<img className="ml-2"
                                                                     src={`${!showTipsBtn ? "/down.svg" : "/up.svg"}`}
                                                                     alt=""/>
                                        </a>
                                    </div>

                                    <div className="flex w-full px-14 py-6 "
                                         style={{
                                             display: (showTipsBtn ? 'block' : 'none'),
                                             background: 'rgba(93,38,113,0.1)'
                                         }}>
                                        <div className="flex">
                                            <p className="lato tablet:text-sm mobile:text-xs">
                                                {tipsForBtnUnoTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }

                        {
                            (roomRef ? roomRef.unoToTheLast : room.unoToTheLast) ?
                                <div>
                                    <div className="flex w-full pt-4 pb-2 tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6 items-center">
                                        <a className="flex w-full  items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                                           onClick={() => (!showUnoLast ?
                                               setShowUnoLast(true) :
                                               setShowUnoLast(false))}>
                                            {unoLastTitle}<img className="ml-2"
                                                               src={`${!showUnoLast ? "/down.svg" : "/up.svg"}`}
                                                               alt=""/>
                                        </a>
                                    </div>

                                    <div className="flex w-full px-14 py-6 "
                                         style={{display: (showUnoLast ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                                        <div className="flex">
                                            <p className="lato tablet:text-sm mobile:text-xs">
                                                {unoLastTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }

                        {
                            (roomRef ? roomRef.wildFourDifficulty : room.wildFourDifficulty) ?
                                <div>
                                    <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                                        <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                                           onClick={() => (!showWildFourDiff ?
                                               setShowWildFourDiff(true) :
                                               setShowWildFourDiff(false))}>
                                            {wildFourDifficultyTitle}<img className="ml-2"
                                                                          src={`${!showWildFourDiff ? "/down.svg" : "/up.svg"}`}
                                                                          alt=""/>
                                        </a>
                                    </div>

                                    <div className="flex w-full px-14 py-6"
                                         style={{
                                             display: (showWildFourDiff ? 'block' : 'none'),
                                             background: 'rgba(93,38,113,0.1)'
                                         }}>
                                        <div className="flex">
                                            <p className="lato tablet:text-sm mobile:text-xs">
                                                {wildFourDifficultyTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }

                        {
                            (roomRef ? roomRef.accum : room.accum) ?
                                <div>
                                    <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                                        <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                                           onClick={() => (!showAccum ?
                                               setShowAccum(true) :
                                               setShowAccum(false))}>
                                            {accumTitle}<img className="ml-2" src={`${!showAccum ? "/down.svg" : "/up.svg"}`}
                                                             alt=""/>
                                        </a>
                                    </div>

                                    <div className="flex w-full px-14 py-6"
                                         style={{display: (showAccum ? 'block' : 'none'), background: 'rgba(93,38,113,0.1)'}}>
                                        <div className="flex">
                                            <p className="lato tablet:text-sm mobile:text-xs">
                                                {accumTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }


                        {
                            (roomRef ? roomRef.dropSameCards : room.dropSameCards) ?
                                <div>
                                    <div className="flex w-full pt-4 pb-2 items-center tablet:pl-10 mobile:pl-6 tablet:pr-10 mobile:pr-6">
                                        <a className="flex w-full items-center cursor-pointer font-bold tablet:text-base mobile:text-sm"
                                           onClick={() => (!showTwoSameCards ?
                                               setShowTwoSameCards(true) :
                                               setShowTwoSameCards(false))}>
                                            {twoSameCardsTitle}<img className="ml-2"
                                                                    src={`${!showTwoSameCards ? "/down.svg" : "/up.svg"}`} alt=""/>
                                        </a>
                                    </div>

                                    <div className="flex w-full px-14 py-6"
                                         style={{
                                             display: (showTwoSameCards ? 'block' : 'none'),
                                             background: 'rgba(93,38,113,0.1)'
                                         }}>
                                        <div className="flex flex-col">
                                            {/*<p className="lato text-sm mb-4 text-gray-dark">
                                {twoSameCardsTxtGray}
                            </p>*/}
                                            <p className="lato tablet:text-sm mobile:text-xs">
                                                {twoSameCardsTxtBlack}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }


                    </div>
                </div>
                <div className="footerModal flex w-full justify-end">
                    <button
                        onClick={rest.onRequestClose}
                        className={styles([
                            "text-white font-bold tablet:text-sm mobile:text-xs leading-btn bg-cyan btnUnderstandSave rounded-def",
                            "focus:outline-none focus:bg-cyan-active focus:text-gray-light focus:shadow-cyan",
                            "hover:bg-cyan-hover hover:shadow-cyan",
                            "duration-150 ease-in-out transition",
                        ])}
                    >
                        Понятно
                    </button>
                </div>
            </div>
        );
}
