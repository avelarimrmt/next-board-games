import React from "react";

export default function BoardLayout({
                                        room,
                                        players,
                                        currentPlayerId,
                                        renderPlayer,
                                        drawPile,
                                        discardPile,
                                        playerOptionsWildCard,
                                        playerOptionsWithPass,
                                        playerOptionsWithUno,
                                        yellOneMessage,
                                        direction,
                                        display
                                    }) {
    const currentPlayer = players.find((player) => player.id === currentPlayerId);
    const indexCurrentPlayer = players.indexOf(currentPlayer);

    return (
        <>
        <div className="flex flex-grow items-center justify-center">
            <div className="absolute z-0">
                {room.isReverse ?
                    (window.innerWidth >= 768 ?
                        <img className="transition delay-700 duration-300 ease-in"
                             src="/uno/gameSession/counterClockwise.svg" alt=""/>
                        : <img className="-mt-12 transition delay-700 duration-300 ease-in"
                               src="/uno/gameSession/counterClockwise2.svg" alt=""/>) :
                    (window.innerWidth >= 768 ?
                        <img className="transition delay-700 duration-300 ease-in"
                             src="/uno/gameSession/clockWise.svg"
                             alt=""/>
                        : <img className="-mt-12 transition delay-700 duration-300 ease-in"
                               src="/uno/gameSession/clockWise2.svg"
                               alt=""/> )}

            </div>
            <div className="z-10 flex absolute bottom-0 w-full justify-center items-center">
                <div className="text-white gridBoard2 grid grid-cols-3 w-full h-screen">
                    {players.map((player, index) => {
                        const isCurrentPlayer = player.id === currentPlayerId;
                        let positionPlayer;
                        players.length === 2
                            ? (positionPlayer = {
                                0: {
                                    grid: "row-start-3 col-start-1 col-span-3 tablet:flex-col mobile:flex-col-reverse h-full",
                                },
                                1: {
                                    grid: "row-start-1 col-start-2 col-span-1 tablet:flex-col-reverse mobile:flex-col h-full",
                                },
                            })
                            : (positionPlayer = {
                                0: {
                                    grid: "row-start-3 col-start-1 col-span-3 tablet:flex-col mobile:flex-col-reverse h-full",
                                },
                                1: {
                                    grid: "row-start-2 col-start-1 col-span-1 tablet:flex-row-reverse mobile:flex-col items-center sideClip justify-between",
                                },
                                2: {
                                    grid: "row-start-1 col-start-2 col-span-1 tablet:flex-col-reverse mobile:flex-col tablet:justify-start mobile:justify-end h-full",
                                },
                                3: {
                                    grid: "row-start-2 col-start-3 col-span-1 tablet:flex-row mobile:flex-col items-center sideClip justify-between",
                                },
                            });
                        const posPlayer =
                            (players.length - indexCurrentPlayer + index) % players.length;

                        return (
                            <div
                                key={player.id}
                                className={`${positionPlayer[posPlayer].grid} flex`}
                            >
                                {renderPlayer(player, isCurrentPlayer)}
                            </div>
                        );
                    })}
                    <div
                        className={`h-full row-start-2 col-start-2 col-span-1 flex ${direction} items-center`}
                    >
                        {
                            <div className="flex flex-initial flex-no-wrap">
                                {drawPile}
                                {discardPile}
                            </div>
                        }

                        <div className={`${playerOptionsWildCard ? null : "hidden"} flex justify-center flex-wrap`}>
                            {playerOptionsWildCard ? playerOptionsWildCard :
                               null
                            }
                        </div>

                        {playerOptionsWithPass === null ? playerOptionsWithUno :
                            (display == "hidden") ? playerOptionsWithUno :
                            <div className={`${display} flex justify-center flex-wrap`}>
                                {
                                    (playerOptionsWithPass)
                                }
                            </div>
                        }

                    </div>
                    <div className="row-start-1 col-start-1 col-span-3 flex flex-col items-center justify-center">
                        {yellOneMessage}
                    </div>
                </div>

            </div>

        </div>
            </>
    );
}