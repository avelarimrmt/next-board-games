import React from "react";
import ReactModal from "react-modal";
import styles from "classnames";

ReactModal.setAppElement("#__next");

export default function ModalCollapse({ children, button1, button2, button3, ...rest }) {
    return (
        <ReactModal {...rest}>
            <div className="relative p-2 shadow mobile:flex mobile:flex-col mobile:justify-center">
                <div className="flex items-center justify-between">
                        {button1}
                        {button2}
                        {button3}
                </div>
            </div>
                {children}
        </ReactModal>
    );
}