import React, {useState} from "react";
import {useRouter} from "next/router";

const LoginGuest: React.FC = () => {
    const [playerName, setPlayerName] = useState("");
    const router = useRouter();
    const [error, setError] = useState("hidden");
    const [focus, setFocus] = useState("border-gray-light focus:shadow-focus-blue focus:border-blue");
    const onSubmit = (event) => {
        event.preventDefault();
        setError("hidden");
        setFocus("border-gray-light focus:shadow-focus-blue focus:border-blue");
        if (playerName) {
            router.push("/uno/rooms");
            if (typeof window !== 'undefined')
                sessionStorage.setItem('name', playerName);
        }
        else {
            setError("block");
            setFocus("focus:shadow-focus-red focus:border-red border-red")
        }
    };
    return (
        <form onSubmit={onSubmit}>
            <div className="pt-6">
                <div className={`empty ${error} flex flex-row rounded-lg items-center py-2.5 px-4`} style={{background: 'rgba(219, 85, 64, 0.1)'}}>
                    <img src="/error.svg" alt=""/>
                    <p className="ml-4 text-red text-sm lato">Имя не может быть пустым</p>
                </div>
                <label
                    htmlFor="name"
                    className="block text-sm lato text-gray-dark mt-6"
                >
                    Имя пользователя
                </label>
                <div className="mt-1 rounded-md">
                    <input
                        id="name"
                        maxLength={25}
                        className={`lato text-sm appearance-none block w-full border rounded-md focus:outline-none ${focus} transition duration-150 ease-in-out`}
                        style={{padding: '0.5625rem 1rem',}}
                        type="text"
                        name="name"
                        value={playerName}
                        onChange={(e) => setPlayerName(e.target.value)}
                    />
                </div>
            </div>

            <div className="mt-8 mb-10">
                <button
                    type="submit"
                    className="w-full flex justify-center btn btnHead text-sm font-bold text-white bg-blue-hover hover:bg-indigo-hover hover:shadow-hover-indigo active:bg-indigo-active active:shadow-active-indigo active:text-gray-light transition duration-150 ease-in-out"
                >
                    Войти
                </button>
            </div>
        </form>
    );
};
export default LoginGuest;