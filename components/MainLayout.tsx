import React, {useState} from "react";
import Link from 'next/link'
import Head from "next/head";
/*import Modal from "./Modal";
import Login from "./Login";
import SignUp from "./SignUp";*/

export function MainLayout({children, color ="transparent", title = 'Лаунчер настольных игр'}) {
    /*const [showModalLogin, setShowModalLogin] = useState(false);
    const [showModalSignUp, setShowModalSignUp] = useState(false);*/
    return (
        <>
            <Head>
                <title>{title}</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <nav className={`w-full bg-${color} mobile:hidden tablet:flex`}>
                <div className="container flex justify-between items-center">
                    <div className="flex items-center text-black-light">
                        <Link href="/">
                            <a className="home font-bold text-lg">Главная</a>
                        </Link>
                    </div>
                    {/*Кнопки входа и регистрации в шапке*/}
                    {/*<div className="flex items-center">
                        <div>
                            <a className="btnSignUp cursor-pointer font-normal text-black text-sm underline mr-6"
                               onClick={() => {
                                   setShowModalSignUp(true);
                               }}>
                                Регистрация
                            </a>
                        </div>
                        <div>
                            <button className="btn btnLogin font-bold text-sm text-white bg-blue-hover"
                                    onClick={() => {
                                        setShowModalLogin(true);
                                    }}>
                                Вход
                            </button>
                        </div>
                    </div>*/}
                </div>
            </nav>
            <main>
                {children}
            </main>
            <footer className="footer w-full justify-center mobile:hidden tablet:flex">
                <a className="lato text-white font-normal m-auto">
                    2020-2021
                </a>
            </footer>

            <style jsx>{`
            .home {
                border-bottom: 2px solid #DB5540;
            }
            
            nav{
                height: 60px;
            }    
            
            .btnSignUp:hover {
                color: #3F5FCE;
            }
            
            .btnSignUp:active {
                color: #2D4BB2;
            }
            
            .btnLogin:hover {
                background: #3859CC;
            
                /* blue shadow hover */
                box-shadow: 0px 5px 15px -6px rgba(63, 95, 206, 0.2), 0px 5px 20px -4px rgba(63, 95, 206, 0.2);
            }

            .btnLogin:active {
                background: #2D4BB2;
                color: #DDE2E5;

                /* blue shadow hover */
                box-shadow: 0px 5px 15px -6px rgba(63, 95, 206, 0.2), 0px 5px 20px -4px rgba(63, 95, 206, 0.2);
            }
            
            .footer {
            height: 60px;
            background: #DB5540;
            }
            `}</style>
            {/*Модальные окна входа и регистрации*/}
            {/* <Modal
                className="modal w-auto"
                contentLabel="Login"
                id="modal-login"
                isOpen={showModalLogin}
                onRequestClose={() => {
                    setShowModalLogin(false);
                }}
                overlayClassName="overlay"
                title={"Вход"}
                subtitle={"Мы рады видеть вас снова!"}
            >
                <Login/>
                <div className="mt-2">
                    <p className="lato text-xs text-black-light">
                        {"Нет учетной записи?"}
                        <a href="#" className="ml-1 underline text-blue-hover"
                           onClick={() => {
                               setShowModalSignUp(true);
                               setShowModalLogin(false);
                           }}>
                            Регистрация
                        </a>
                    </p>
                </div>
            </Modal>

            <Modal
                className="modal w-auto"
                contentLabel="SignUp"
                id="modal-signup"
                isOpen={showModalSignUp}
                onRequestClose={() => {
                    setShowModalSignUp(false);
                }}
                overlayClassName="overlay"
                title={"Регистрация"}
                subtitle={["Для доступа к играм вам",[<br/>],"необходимо зарегистрироваться."]}
            >
                <SignUp/>
                <div className="mt-2">
                    <p className="lato text-xs text-black-light">
                        {"Уже зарегистрированы?"}
                        <a href="#" className="ml-1 underline text-blue-hover"
                           onClick={() => {
                               setShowModalLogin(true);
                               setShowModalSignUp(false);
                           }}>
                            Вход
                        </a>
                    </p>
                </div>
            </Modal>*/}
        </>
    )
}
