import React from "react";


const Login: React.FC = () => {
    return (
        <form>
            <div>
                <label
                    htmlFor="email"
                    className="block text-sm lato text-gray-dark mt-10"
                >
                    Имя пользователя или E-mail
                </label>
                <div className="mt-1 rounded-md">
                    <input
                        id="email"
                        className="lato text-sm appearance-none block w-full border border-gray-light rounded-md focus:outline-none focus:shadow-focus-blue focus:border-blue-hover transition duration-150 ease-in-out"
                        style={{padding: '0.5625rem 1rem',}}
                        type="email"
                        name="email"
                    />
                </div>
            </div>
            <div className="mt-2">
                <label
                    htmlFor="password"
                    className="block text-sm lato text-gray-dark"
                >
                    Пароль
                </label>
                <div className="mt-1 rounded-md">
                    <input
                        id="password"
                        className="lato text-sm appearance-none block w-full border border-gray-light rounded-md focus:outline-none focus:shadow-focus-blue focus:border-blue-hover transition duration-150 ease-in-out"
                        style={{padding: '0.5625rem 1rem',}}
                        type="password"
                        name="password"
                    />
                </div>
            </div>
            <div className="mt-3 flex items-center">

                <div className="flex items-center">
                    <input
                        type="checkbox"
                        id="remember"
                        className="mr-2"
                    />
                </div>
                <label htmlFor="remember"
                       className="lato text-sm cursor-pointer"
                       style={{color: '252525',}}
                >
                    Запомнить меня
                </label>


            </div>
            <div className="mt-6">
                <button
                    type="submit"
                    className="w-full flex justify-center btn btnHead text-sm font-bold text-white bg-blue-hover hover:bg-indigo-hover hover:shadow-hover-indigo active:bg-indigo-active active:shadow-active-indigo active:text-gray-light transition duration-150 ease-in-out"
                >
                    Войти
                </button>
            </div>
        </form>
    );
};
export default Login;