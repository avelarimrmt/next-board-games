module.exports = {
  theme: {
    extend: {
      colors: {
        blue: {
          light: '#D7DFFB',
          default: '#3F5FCE',
          dark: '#190F2F',
          hover: '#3859CC',
          active: '#2D4BB2',
        },
        black: {
          light: '#252525',
        },
        gray: {
          light: '#DDE2E5',
          default: '#C4C4C4',
          dark: '#808080',
          lightest: '#F5F5F5',
        },
        purple: {
          light: '#463062',
          default: '#5D2671',
          dark: '#200F38',
          hover: '#521F65',
          active: '#4E1B60',
          darkest: '#3D2B55',
        },
        violet: {
          hover: '#F6DCFF',
          active: '#E9D1F1',
        },
        orange: {
          default: '#E9A527',
        },
        yellow: {
          default: '#E89822',
          hover: '#DF9321',
          active: '#D08A22',
        },
        cyan: {
          default: '#27DC95',
          hover: '#23D48F',
          active: '#22B77C',
          checked: '#27DC95',
        },
        indigo: {
          default: '#3F5FCE',
          hover: '#3859CC',
          active: '#2E4FBF',
        },
        red: {
          default: '#DB5540',
          hover: '#CF4F3B',
          active: '#BE422E',
        },
        pink: {
          default: '#FBEEEC',
          light: '#FFEBE8',
        },
      },

      screens: {
        'mobile': '320px',
        'tablet': '768px',
        'desktop': '1024px',
      },

      fontSize: {
        'tiny': '1.0625rem',
        'mxl': '1.375rem',
        '2.5xl': '1.75rem',
        '4.5xl': '2.625rem',
        '2rem': '2rem',
        'over': '1.625rem',
        'xxs': '0.5rem',
      },

      lineHeight: {
        'rules': '1.875rem',
        'desc': '1,375rem',
        'btn': '1.5625rem',
        '12': '3rem',
      },

      width: {
        '53': '53%',
        '48': '48%',
        '35': '35%',
        '15': '3.75rem',
        '23': '5.8125rem',
        '27': '6.625rem',
        'discard': '5.4375rem',
        'back': '3.0625rem',
        'player': '6.875rem',
        'winner': '5.625rem',
        'sum': '15.25rem',
        'number': '6.75rem',
        'lose': '23rem',
        '13': '3.8rem',
        '2.5': '1.625rem',
      },

      height: {
        '15': '3.75rem',
        'player': '7.75rem',
        'playerM': '5.1875rem',
        'winner': '5.625rem',
        '75': '75%',
        '33': '8.125rem',
      },

      opacity: {
        '10': '0.1',
        '30': '0.3',
        '15': '0.15',
      },

      padding: {
        '3.5': '0.875rem',
        '2.5': '0.625rem',
        '13': '3.25rem',
        '4.5': '1.125rem',
        '14': '3.5rem',
        '22': '5.5rem',
        '25': '6.5rem',
      },

      margin: {
        '2.5': '0.625rem',
        '0.5': '0.125rem',
        '5.5': '1.375rem',
        '10.5': '2.625rem',
        '1.5': '0.375rem',
      },

      borderRadius: {
        'def': '0.3125rem',
        '2.5xl': '1.25rem',
        'lg': '0.5rem',
      },
      borderWidth: {
        '5': '0.3125rem',
      },

      inset: {
        '15': '3.75rem',
        '0.5': '8vh',
        '2.5': '0.625rem',
        '5': '1.25rem',
        '17': '4.1rem',
        '10': '2.5rem',
        '96': '6rem',
        '2/4': {
          left: '50%',
          right: '50%',
        },
      },


    },

    container: {
      center: true,
    },

    boxShadow: {
      cyan: '0px 5px 15px -6px rgba(39, 220, 149, 0.2), 0px 5px 20px -4px rgba(39, 220, 149, 0.2)',
      violet: ' 0px 5px 15px -6px rgba(93, 38, 113, 0.2), 0px 5px 20px -4px rgba(93, 38, 113, 0.2)',
    },

    customForms: theme => ({
      default: {
        checkbox: {
          '&:focus': {
            boxShadow: undefined,
            borderColor: undefined,
          },
        },
      }
    })
  },
  variants: {
    extend: {
    }
  },
  plugins: [
    require('@tailwindcss/custom-forms'),
  ],
};