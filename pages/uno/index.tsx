import {MainLayout} from "../../components/MainLayout";
import React, {useState} from "react";
import styles from "../../styles/Home.module.css";
import Modal from "../../components/Modal";
import LoginGuest from "../../components/LoginGuest";
import {
    actionCardInDiscardTxt,
    actionCardTxt, andFurtherTitle, andFurtherTxt,
    descOfGameTitle,
    drawTwoTitle,
    drawTwoTxt, exampleTitle, exampleTxt,
    goalOfGameTitle,
    goalOfGameTxt, howToMoveTitle, howToMoveTxt, howToWinTitle, howToWinTxt,
    ordinaryCardTxt,
    reverseTitle,
    reverseTxt, scoringTitle, scoringTxt,
    skipTitle,
    skipTxt,
    wildCardTitle,
    wildCardTxt,
    wildCarsTxt,
    wildFourTitle,
    wildFourTxt,

} from "../../utils/unoRules";
import Link from "next/link";

export function DescriptionUno() {
    const [showModal, setShowModal] = useState(false);
    const [showRules, setShowRules] = useState(false);
    return (
        <MainLayout title={'UNO'} color={'gray-lightest'}>
            <main className="bg-gray-lightest tablet:h-auto mobile:h-screen w-full flex-col">
                <div className="w-full tablet:pt-10 bg-gray-lightest">
                    <div className="gameCover relative">
                        <img src="../uno/banner.jpg" alt=""/>
                        <div className="mobile:block tablet:hidden absolute" style={{top: '40px', left: '20px', }}>
                            <Link href="/">
                                <button>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M21.284 10.7151H6.03678L10.7035 5.10993C10.9217 4.84739 11.0267 4.50892 10.9954 4.16897C10.964 3.82902 10.7989 3.51545 10.5364 3.29724C10.2738 3.07902 9.93536 2.97404 9.59542 3.00538C9.25547 3.03672 8.9419 3.20182 8.72368 3.46436L2.29569 11.178C2.25244 11.2393 2.21377 11.3038 2.17998 11.3708C2.17998 11.4351 2.17998 11.4736 2.08999 11.5379C2.03172 11.6853 2.00121 11.8422 2 12.0007C2.00121 12.1592 2.03172 12.3162 2.08999 12.4636C2.08999 12.5278 2.08999 12.5664 2.17998 12.6307C2.21377 12.6977 2.25244 12.7622 2.29569 12.8235L8.72368 20.5371C8.84456 20.6822 8.99592 20.799 9.16702 20.8789C9.33811 20.9589 9.52473 21.0002 9.71359 20.9999C10.014 21.0005 10.3051 20.8959 10.5364 20.7042C10.6666 20.5963 10.7742 20.4638 10.853 20.3142C10.9319 20.1646 10.9805 20.001 10.996 19.8326C11.0116 19.6642 10.9937 19.4944 10.9435 19.3329C10.8933 19.1714 10.8118 19.0215 10.7035 18.8916L6.03678 13.2863H21.284C21.6249 13.2863 21.9519 13.1509 22.193 12.9098C22.4341 12.6687 22.5696 12.3417 22.5696 12.0007C22.5696 11.6598 22.4341 11.3328 22.193 11.0917C21.9519 10.8506 21.6249 10.7151 21.284 10.7151Z" fill="black"/>
                                    </svg>
                                </button>
                            </Link>
                        </div>

                    </div>

                    <div className="container2 mt-10 flex tablet:flex-row mobile:flex-col justify-between tablet:items-center mobile:items-start">
                        <div className="flex flex-row items-end mobile:items-center mobile:ml-4 tablet:ml-0">
                            <div
                                className="flex gameIcon border-2 border-gray-light bg-gray-light justify-center items-center">
                                <img src="../uno/iconUno.svg" alt=""/>
                            </div>

                            <div className="flex flex-col ml-6">
                                <h1 className="font-extrabold text-4xl text-blue leading-8"
                                    style={{letterSpacing: '0.3125rem',}}>UNO</h1>

                                <div className={`tablet:flex tablet:flex-row mobile:hidden`}>
                                    <p className="lato text-sm text-gray-dark font-medium">Количество игроков:<span
                                        className="font-semibold text-black-light"> 2–10</span></p>

                                </div>

                                <div className="tablet:flex mobile:hidden">
                                    <p className="lato text-sm text-gray-dark font-medium">Сложность:</p>
                                    <div className="flex flex-row items-center"
                                         style={{marginLeft: '0.3125rem',}}>
                                        <span className={`${styles.star1} pr-2`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                        <span className={`${styles.star1} pr-2`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                        <span className={`${styles.star1}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="tablet:hidden mobile:flex mobile:flex-col mt-4 mb-6 ml-4">
                            <div className={`flex flex-row mb-4`}>
                                <p className="lato text-sm text-gray-dark font-medium">Количество игроков:<span
                                    className="font-semibold text-black-light"> 2–10</span></p>

                            </div>

                            <div className="flex">
                                <p className="lato text-sm text-gray-dark font-medium">Сложность:</p>
                                <div className="flex flex-row items-center"
                                     style={{marginLeft: '0.3125rem',}}>
                                        <span className={`${styles.star1} pr-2`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                    <span className={`${styles.star1} pr-2`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                    <span className={`${styles.star1}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                </div>
                            </div>
                        </div>
                        <div className="tablet:flex mobile:block mobile:w-full tablet:w-auto">
                            <div>
                                <button type="button"
                                        className={`mobile:w-full btn btnPlay bg-blue hover:bg-blue-hover active:bg-blue-active text-white font-bold text-lg`}

                                        onClick={() => {
                                            setShowModal(true);
                                        }}>
                                    Играть
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="container2 flex flex-col text-black-light">
                        <div className="mobile:hidden tablet:flex mt-10">
                            <h2 className="font-extrabold text-2xl text-blue leading-8">Правила</h2>
                        </div>

                        <a className="mobile:flex tablet:hidden w-full items-center cursor-pointer font-bold text-base mt-10"
                           onClick={() => (!showRules ?
                               setShowRules(true) :
                               setShowRules(false))}>
                            <h2 className="font-extrabold text-2xl text-blue leading-8">Правила</h2>
                            <img className="ml-2"
                                 src={`${!showRules ? "/down.svg" : "/up.svg"}`}
                                 alt=""/>
                        </a>

                        <div className={`mobile:flex mobile:flex-col tablet:hidden}`}
                             style={{
                                 display: (showRules ? 'block' : 'none'),
                             }}>
                        <div className="flex mt-6">
                            <p className="lato text-tiny leading-rules">
                                Добро пожаловать в УНО – одну из самых популярных настольных игр в мире!<br/>
                                <span
                                    className="nunito font-extrabold text-lg text-yellow">{goalOfGameTitle} –</span> {goalOfGameTxt}
                            </p>
                        </div>

                        <div className="flex mt-6">
                            <p className="lato text-tiny leading-rules">
                                    <span
                                        className="nunito font-extrabold text-lg text-yellow">{descOfGameTitle}:</span><br/>
                                {ordinaryCardTxt}
                            </p>
                        </div>

                        <div className="flex mt-4 justify-center">
                            <div className="card flex mr-3">
                                <img src="../uno/red.jpg" alt=""/>
                            </div>
                            <div className="card flex mr-3">
                                <img src="../uno/yellow.jpg" alt=""/>
                            </div>
                            <div className="card flex mr-3">
                                <img src="../uno/green.jpg" alt=""/>
                            </div>
                            <div className="card flex">
                                <img src="../uno/blue.jpg" alt=""/>
                            </div>
                        </div>

                        <div className="flex mt-8">
                            <p className="lato text-tiny leading-rules">{actionCardTxt}</p>
                        </div>

                        <div className="flex mt-3">
                            <p className="lato text-tiny leading-rules">
                                {actionCardInDiscardTxt}
                            </p>
                        </div>

                        <div className="flex mt-4 pl-4">
                            <div className="flex flex-col">
                                <div className="flex flex-row mb-6">
                                    <div className="card flex">
                                        <img src="../uno/drawTwo.jpg" alt=""/>
                                    </div>
                                    <div className="flex w-2/3 items-center ml-8">
                                        <p className="lato leading-rules">
                                            <span className="font-bold text-lg">{drawTwoTitle}</span><br/>
                                            {drawTwoTxt}
                                        </p>
                                    </div>
                                </div>

                                <div className="flex flex-row mb-6">
                                    <div className="card flex">
                                        <img src="../uno/reverse.jpg" alt=""/>
                                    </div>
                                    <div className="flex w-2/3 items-center ml-8">
                                        <p className="lato leading-rules">
                                            <span className="font-bold text-lg">{reverseTitle}</span><br/>
                                            {reverseTxt}
                                        </p>
                                    </div>
                                </div>

                                <div className="flex flex-row">
                                    <div className="card flex">
                                        <img src="../uno/skip.jpg" alt=""/>
                                    </div>
                                    <div className="flex w-2/3 items-center ml-8">
                                        <p className="lato leading-rules">
                                            <span className="font-bold text-lg">{skipTitle}</span><br/>
                                            {skipTxt}
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="flex mt-8">
                            <p className="lato text-tiny leading-rules">
                                {wildCarsTxt}
                            </p>
                        </div>

                        <div className="flex mt-4 pl-4">
                            <div className="flex flex-col">
                                <div className="flex flex-row mb-6">
                                    <div className="card flex">
                                        <img src="../uno/drawColor.jpg" alt=""/>
                                    </div>
                                    <div className="flex w-2/3 items-center ml-8">
                                        <p className="lato leading-rules">
                                            <span className="font-bold text-lg">{wildCardTitle}</span><br/>
                                            {wildCardTxt}
                                        </p>
                                    </div>
                                </div>

                                <div className="flex flex-row">
                                    <div className="card flex">
                                        <img src="../uno/drawFour.jpg" alt=""/>
                                    </div>
                                    <div className="flex w-2/3 items-center ml-8">
                                        <p className="lato leading-rules">
                                            <span className="font-bold text-lg">{wildFourTitle}</span><br/>
                                            {wildFourTxt}
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="flex mt-8">
                            <p className="lato text-tiny leading-rules">
                                <span
                                    className="nunito font-extrabold text-lg text-yellow">{howToMoveTitle}</span><br/>
                                {howToMoveTxt}
                            </p>
                        </div>

                        <div className="flex mt-4 bg-yellow bg-opacity-10 rounded-lg py-4 px-6">
                            <p className="lato text-tiny leading-rules"><span
                                className="font-bold text-lg">{exampleTitle} &nbsp;</span>&nbsp; &nbsp;
                                {exampleTxt}</p>
                        </div>

                        <div className="flex mt-8">
                            <p className="lato text-tiny leading-rules">
                                    <span className="nunito font-extrabold text-lg text-yellow">
                                        {howToWinTitle}</span><br/>
                                {howToWinTxt}
                            </p>
                        </div>

                        <div className="flex mt-8">
                            <p className="lato text-tiny leading-rules">
                                    <span className="nunito font-extrabold text-lg text-yellow">
                                        {scoringTitle}</span><br/>
                                {scoringTxt}
                            </p>
                        </div>

                        <div className="flex mt-4 justify-center">
                            <table className="table">
                                <tbody className="lato text-tiny leading-rules">
                                <tr>
                                    <td>Все карты с цифрами (0-9)</td>
                                    <td>Подсчет по цифрам на картах</td>
                                </tr>
                                <tr>
                                    <td>Активные карты</td>
                                    <td>20 баллов</td>
                                </tr>
                                <tr>
                                    <td>Черные активные карты</td>
                                    <td>50 баллов</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className="flex mt-8 mb-4">
                            <p className="lato text-tiny leading-rules">
                                    <span className="nunito font-extrabold text-lg text-yellow">
                                        {andFurtherTitle}</span><br/>
                                {andFurtherTxt}
                            </p>
                        </div>
                        </div>

                        <div className="mobile:hidden tablet:flex tablet:flex-col">
                            <div className="flex mt-6">
                                <p className="lato text-tiny leading-rules">
                                    Добро пожаловать в УНО – одну из самых популярных настольных игр в мире!<br/>
                                    <span
                                        className="nunito font-extrabold text-lg text-yellow">{goalOfGameTitle} –</span> {goalOfGameTxt}
                                </p>
                            </div>

                            <div className="flex mt-6">
                                <p className="lato text-tiny leading-rules">
                                    <span
                                        className="nunito font-extrabold text-lg text-yellow">{descOfGameTitle}:</span><br/>
                                    {ordinaryCardTxt}
                                </p>
                            </div>

                            <div className="flex mt-4 justify-center">
                                <div className="card flex mr-5">
                                    <img src="../uno/red.jpg" alt=""/>
                                </div>
                                <div className="card flex mr-5">
                                    <img src="../uno/yellow.jpg" alt=""/>
                                </div>
                                <div className="card flex mr-5">
                                    <img src="../uno/green.jpg" alt=""/>
                                </div>
                                <div className="card flex">
                                    <img src="../uno/blue.jpg" alt=""/>
                                </div>
                            </div>

                            <div className="flex mt-8">
                                <p className="lato text-tiny leading-rules">{actionCardTxt}</p>
                            </div>

                            <div className="flex mt-3">
                                <p className="lato text-tiny leading-rules">
                                    {actionCardInDiscardTxt}
                                </p>
                            </div>

                            <div className="flex mt-4 pl-20">
                                <div className="flex flex-col">
                                    <div className="flex flex-row mb-6">
                                        <div className="card flex">
                                            <img src="../uno/drawTwo.jpg" alt=""/>
                                        </div>
                                        <div className="flex w-53 items-center ml-8">
                                            <p className="lato leading-rules">
                                                <span className="font-bold text-lg">{drawTwoTitle}</span><br/>
                                                {drawTwoTxt}
                                            </p>
                                        </div>
                                    </div>

                                    <div className="flex flex-row mb-6">
                                        <div className="card flex">
                                            <img src="../uno/reverse.jpg" alt=""/>
                                        </div>
                                        <div className="flex w-53 items-center ml-8">
                                            <p className="lato leading-rules">
                                                <span className="font-bold text-lg">{reverseTitle}</span><br/>
                                                {reverseTxt}
                                            </p>
                                        </div>
                                    </div>

                                    <div className="flex flex-row">
                                        <div className="card flex">
                                            <img src="../uno/skip.jpg" alt=""/>
                                        </div>
                                        <div className="flex w-53 items-center ml-8">
                                            <p className="lato leading-rules">
                                                <span className="font-bold text-lg">{skipTitle}</span><br/>
                                                {skipTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="flex mt-8">
                                <p className="lato text-tiny leading-rules">
                                    {wildCarsTxt}
                                </p>
                            </div>

                            <div className="flex mt-4 pl-20">
                                <div className="flex flex-col">
                                    <div className="flex flex-row mb-6">
                                        <div className="card flex">
                                            <img src="../uno/drawColor.jpg" alt=""/>
                                        </div>
                                        <div className="flex w-48 items-center ml-8">
                                            <p className="lato leading-rules">
                                                <span className="font-bold text-lg">{wildCardTitle}</span><br/>
                                                {wildCardTxt}
                                            </p>
                                        </div>
                                    </div>

                                    <div className="flex flex-row">
                                        <div className="card flex">
                                            <img src="../uno/drawFour.jpg" alt=""/>
                                        </div>
                                        <div className="flex w-35 items-center ml-8">
                                            <p className="lato leading-rules">
                                                <span className="font-bold text-lg">{wildFourTitle}</span><br/>
                                                {wildFourTxt}
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className="flex mt-8">
                                <p className="lato text-tiny leading-rules">
                                <span
                                    className="nunito font-extrabold text-lg text-yellow">{howToMoveTitle}</span><br/>
                                    {howToMoveTxt}
                                </p>
                            </div>

                            <div className="flex mt-4 bg-yellow bg-opacity-10 rounded-lg py-3.5 px-13">
                                <p className="lato text-tiny leading-rules"><span
                                    className="font-bold text-lg">{exampleTitle} &nbsp;</span>&nbsp; &nbsp;
                                    {exampleTxt}</p>
                            </div>

                            <div className="flex mt-8">
                                <p className="lato text-tiny leading-rules">
                                    <span className="nunito font-extrabold text-lg text-yellow">
                                        {howToWinTitle}</span><br/>
                                    {howToWinTxt}
                                </p>
                            </div>

                            <div className="flex mt-8">
                                <p className="lato text-tiny leading-rules">
                                    <span className="nunito font-extrabold text-lg text-yellow">
                                        {scoringTitle}</span><br/>
                                    {scoringTxt}
                                </p>
                            </div>

                            <div className="flex mt-4 justify-center">
                                <table className="table">
                                    <tbody className="lato text-tiny leading-rules">
                                    <tr>
                                        <td>Все карты с цифрами (0-9)</td>
                                        <td>Подсчет по цифрам на картах</td>
                                    </tr>
                                    <tr>
                                        <td>Активные карты</td>
                                        <td>20 баллов</td>
                                    </tr>
                                    <tr>
                                        <td>Черные активные карты</td>
                                        <td>50 баллов</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div className="flex mt-8">
                                <p className="lato text-tiny leading-rules">
                                    <span className="nunito font-extrabold text-lg text-yellow">
                                        {andFurtherTitle}</span><br/>
                                    {andFurtherTxt}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    className="modal w-auto"
                    contentLabel="Login"
                    id="modal-login"
                    isOpen={showModal}
                    onRequestClose={() => {
                        setShowModal(false);
                    }}
                    overlayClassName="overlay overlayModal"
                    title={"Вход"}
                    subtitle={"Введите имя для входа в игру"}
                >
                    <LoginGuest/>
                </Modal>
            </main>
            <style jsx>{`
            
            .gameIcon {
                width: 5.125rem;
                height:  5.125rem;
                border-radius: 0.625rem;
            }
            
            .bgCarousel {
                border-radius: 5px;
            }
            
            @media (min-width: 320px) {
                .card {
                width: 4.375rem;
                height: 6.25rem;
                }
                .btnPlay {
                padding: 1rem 0;
                }
            }
            
            @media (min-width: 768px) {
                .card {
                width: 6.25rem;
                height: 8.75rem;
                }
                .gameCover img {
                border-radius: 0.625rem;
                }
                .btnPlay {
                padding: 0.375rem 1.5rem;
                }
            }
    
            main {
                padding-bottom: 4.125rem;
            }
            
            table {
                border: 2px solid #E89822;
                border-radius: 0.625rem;
                display: block;
            }
            
            td {
                padding: 0.3125rem 1rem;
            }
            
            tr {
                border-bottom: 2px solid #E89822;
            }
            
            tr:nth-last-child(1) {
                border-bottom: none;
            }
            
            td:nth-child(odd) {
                border-right: 2px solid #E89822;
            }
            
            .btnPlay:hover {
            /* blue shadow hover */
            box-shadow: 0 5px 15px -6px rgba(63, 95, 206, 0.2), 0 5px 20px -4px rgba(63, 95, 206, 0.2);
            }

            .btnPlay:active {
            color: #DDE2E5;

            /* blue shadow hover */
            box-shadow: 0 5px 15px -6px rgba(63, 95, 206, 0.2), 0 5px 20px -4px rgba(63, 95, 206, 0.2);
            }
            `}
            </style>
        </MainLayout>
    )
}

export default DescriptionUno;