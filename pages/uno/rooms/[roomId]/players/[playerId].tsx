import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import db from "../../../../../config/firebase";
import StartGame from "../../../../../components/StartGame";
import {isDrawTwo, isWild, takeACard} from "../../../../../utils/game";
import Main from "../../../../../components/Main";
import Heading from "../../../../../components/Heading";
import Button from "../../../../../components/Button";
import Layout from "../../../../../components/Layout";
import firebase from "firebase";
import ModalRules from "../../../../../components/ModalRules";
import Rules from "../../../../../components/Rules";
import SpecialRules from "../../../../../components/SpecialRules";

const Game: React.FC = () => {

    const [room, setRoom] = useState(null);
    const [playersActive, setPlayersActive] = useState([]);
    const [userPlayers, setUserPlayers] = useState([]);
    const [playersInRoom, setPlayersInRoom] = useState([]);
    const [playersWithCards, setPlayersWithCards] = useState([]);
    const router = useRouter();
    const roomId = router.query.roomId;
    const playerId = router.query.playerId;

    const [showModalGame, setShowModalGame] = useState(false);
    const [showModalRoom, setShowModalRoom] = useState(false);


    useEffect(() => {
        if (roomId) {
            const roomRef = db.collection("rooms").doc(roomId.toString());

            const roomUnsubscribe = roomRef.onSnapshot((roomRef) => {
                setRoom(roomRef.data());
            });

            const playersUnsubscribe = roomRef
                .collection("players")
                .onSnapshot(function (querySnapshot) {
                    const players = [];
                    const users = [];
                    const withCards = [];
                    const playersExit = [];

                    querySnapshot.forEach(function (doc) {
                        players.push(doc);
                        if (doc.data().admin == false) {
                            users.push(doc);
                        }
                        if (doc.data().cards.length !== 0 )
                        {
                            withCards.push(doc);
                        }

                        if (doc.data().isExit == true)
                        {
                            playersExit.push(doc);
                        }
                        else
                            playersExit.push(undefined);
                    });
                    setPlayersActive(players);
                    setUserPlayers(users);
                    setPlayersWithCards(withCards);
                    setPlayersInRoom(playersExit);
                });
            return () => {
                roomUnsubscribe();
                playersUnsubscribe();
            };
        }
    }, [roomId]);

    const onNewGame = (e) => {
        event.preventDefault();
        const roomRef = db.collection("rooms").doc(roomId.toString());
        let usedCards = {};
        let firstCard = takeACard(usedCards);

        while (isWild(firstCard)) {
            usedCards = {};
            firstCard = takeACard(usedCards);
        }
        const drawCount = isDrawTwo(firstCard) ? 2 : 0;
        playersActive.forEach((playerActive) => {
            const cards = [];
            const number = room.number;
            for (let i = 1; i <= number; i++) {
                const card = takeACard(usedCards);
                cards.push(card);
            }

            playerActive.ref.set(
                {
                    cards: cards,
                    isDrop: false,
                    winner: false,
                },
                {merge: true}
            );
        });

        roomRef.set(
            {
                playing: true,
                discardPile: firstCard,
                currentMove: 0,
                deckDict: usedCards,
                isReverse: false,
                drawPile: false,
                drawCount: drawCount,
                previousMove: null,
                yellOne: null,
                pennalty: null,
                isSame: false,
                countSame: 0,
                winners: [],
            },
            {merge: true}
        );
    };

    const currentPlayer = playersActive.find((player) => player.id == playerId);
    const indexCurrentPlayer = playersActive.indexOf(currentPlayer);

    if (!room) {
        return (
            <Main color={"purple"}>
                <Layout playersActive={playersActive}
                        room={null}>
                    <Heading color="white">
                        Loading...
                    </Heading>
                </Layout>
            </Main>
        );
    }

    const roomRef = db.collection("rooms").doc(roomId.toString());
    const playerRef = roomRef.collection("players").doc(playerId.toString());

    let adminPlayer;
    if (playerId) {
        playersActive.map((player) => {
            const isAdmin =
                player.data().admin == true && player.id == playerId;
            const user = player.data().admin == false && player.id == playerId;
            return isAdmin ? adminPlayer = true :
                user ? adminPlayer = false : null;
        })
    }

    let isExistPlayer;
    if (currentPlayer)
        isExistPlayer = currentPlayer.data().isExit === false;
    else isExistPlayer = true;


    if (room.playing && isExistPlayer) {
        return (
            <Main color={"purple"}>
                <Layout admin={adminPlayer ? adminPlayer : null}
                        playersActive={playersActive}
                        userPlayers={userPlayers}
                        playersWithCards={playersWithCards.length === 0 ? playersActive : playersWithCards}
                        room={room}>
                    <StartGame
                        admin={adminPlayer ? adminPlayer : null}
                        room={room}
                        roomId={roomId}
                        playersActive={playersActive}
                        playersWithCards={playersWithCards.length === 0 ? playersActive : playersWithCards}
                        userPlayers={userPlayers}
                        playerId={playerId}
                    />
                </Layout>
            </Main>
        );
    } else {
        if (room.playing && playersActive && playerRef && !playersActive.find(
            (player) => player.data().isExit == false)) {
            playerRef.update({
                isDrop: false,
                isExit: false,
            });

            roomRef.update({
                playing: false,
                currentMove: firebase.firestore.FieldValue.delete(),
                deckDict: firebase.firestore.FieldValue.delete(),
                discardColor: firebase.firestore.FieldValue.delete(),
                discardPile: firebase.firestore.FieldValue.delete(),
                isReverse: firebase.firestore.FieldValue.delete(),
                drawPile: firebase.firestore.FieldValue.delete(),
                drawCount: firebase.firestore.FieldValue.delete(),
                yellOne: firebase.firestore.FieldValue.delete(),
                pennalty: firebase.firestore.FieldValue.delete(),
                previousMove: firebase.firestore.FieldValue.delete(),
                winners: [],
            });
        }
        if (!room.playing)
            playerRef.update({
                isDrop: false,
                isExit: false,
            });
        const waitStartGame = !room.playing && isExistPlayer;
        let players = [];
        if (waitStartGame)
            players = playersActive;
        else players = playersInRoom;

        return (
            <Main>
                <Layout size={!room.playing ? playersActive.length : null}
                        admin={adminPlayer ? adminPlayer : null}
                        playersActive={playersActive}
                        userPlayers={userPlayers}
                        room={!room.playing || (room.playing && currentPlayer.data().isExit === true) ? null : room}
                        roomRef={room}
                >
                    <div className="flex flex-grow boardTable">
                        <div className="flex absolute bottom-0 w-full justify-center items-center">
                            <div className="gridBoard grid text-white grid-cols-3 w-full h-screen">
                                {players.map((player, index) => {
                                    let positionPlayer;
                                    (positionPlayer = {
                                        0: {
                                            grid: "row-start-3 col-start-1 col-span-3 tablet:flex-col mobile:flex-col-reverse h-full",
                                        },
                                        1: {
                                            grid: "row-start-2 col-start-1 col-span-1 flex items-center justify-end sideClip",
                                        },
                                        2: {
                                            grid: "row-start-1 col-start-2 col-span-1 flex-col-reverse h-full",
                                        },
                                        3: {
                                            grid: "row-start-2 col-start-3 col-span-1 flex items-center justify-start sideClip",
                                        },
                                    });
                                    const posPlayer =
                                        (players.length - indexCurrentPlayer + index) % players.length;

                                    return (
                                        <div
                                            key={player ? player.id : null}
                                            className={`${positionPlayer[posPlayer].grid} flex`}
                                        >
                                            {player ?
                                            <div className="playerOnTable justify-center items-center">
                                                <div className="h-full flex flex-col flex-grow-0 justify-center items-center relative">
                                                    {player && player.id === playerId
                                                            ? <div
                                                            className={`z-10 relative avatar avatar${player ? player.data().avatar : null} rounded-2.5xl border-5 w-15 h-15 tablet:mb-2.5 mobile:mb-2`}>
                                                            {player && player.data().admin == true ?
                                                                <img className="adminIcon absolute"
                                                                     src="/uno/gameSession/settings.svg" alt=""/>
                                                                : null}
                                                                </div>
                                                            : <div
                                                                className={`z-10 relative avatar avatar${player ? player.data().avatar : null} tablet:rounded-2.5xl tablet:border-5 tablet:w-15 tablet:h-15 mobile:rounded-2xl mobile:border-4 mobile:w-10 mobile:h-10 tablet:mb-2.5 mobile:mb-2`}>
                                                                {player && player.data().admin == true ?
                                                                    <img className="adminIcon absolute"
                                                                         src="/uno/gameSession/settings.svg" alt=""/>
                                                                    : null}
                                                            </div>
                                                    }
                                                    <div
                                                        className="z-10 nick rounded-lg flex items-center justify-center">
                                                        {player ?
                                                            <p className="truncate font-bold tablet:text-sm mobile:text-xs text-white py-0 px-4">{player.data().name}</p> : null}
                                                    </div>
                                                    {player && player.id === playerId ?
                                                    <div className="tablet:hidden mobile:block absolute bottom-0 h-75 w-full bg-purple-darkest">
                                                        <div className="h-full w-full flex flex-row items-center justify-start text-xxs lato text-white">
                                                            <div className="flex items-center">
                                                            <button className="flex flex-col items-center focus:outline-none ml-5"
                                                                    id="rulesGame"
                                                                    onClick={() => {
                                                                        setShowModalGame(true);
                                                                    }}>
                                                                <div className=" flex justify-end">
                                                                    <svg className="iconInfo" width="20" height="24" viewBox="0 0 20 24" fill="none"
                                                                         xmlns="http://www.w3.org/2000/svg">
                                                                        <path className="pathInfo"
                                                                              d="M2.22266 2.18164H10.556L15.0004 4.36346L17.7782 8.7271V21.818H2.22266V2.18164Z"
                                                                              fill="#DDE2E5"/>
                                                                        <path
                                                                            d="M9.99975 14.3635C9.6805 14.3635 9.37434 14.488 9.1486 14.7096C8.92286 14.9312 8.79604 15.2318 8.79604 15.5453V17.9089C8.79604 18.2224 8.92286 18.523 9.1486 18.7446C9.37434 18.9662 9.6805 19.0907 9.99975 19.0907C10.319 19.0907 10.6252 18.9662 10.8509 18.7446C11.0766 18.523 11.2035 18.2224 11.2035 17.9089V15.5453C11.2035 15.2318 11.0766 14.9312 10.8509 14.7096C10.6252 14.488 10.319 14.3635 9.99975 14.3635ZM10.4572 10.9125C10.2393 10.8134 9.99612 10.7804 9.75901 10.818L9.54234 10.8889L9.32567 10.9953L9.14512 11.1371C9.03063 11.2503 8.94068 11.3851 8.88069 11.5334C8.8207 11.6817 8.7919 11.8403 8.79604 11.9998C8.79513 12.1554 8.82549 12.3095 8.88539 12.4535C8.9453 12.5975 9.03356 12.7285 9.14512 12.8389C9.25959 12.9465 9.39458 13.0308 9.54234 13.0871C9.68519 13.1533 9.84185 13.1856 9.99975 13.1816C10.1582 13.1825 10.3152 13.1527 10.4619 13.0939C10.6085 13.0351 10.7419 12.9484 10.8544 12.8389C10.9659 12.7285 11.0542 12.5975 11.1141 12.4535C11.174 12.3095 11.2044 12.1554 11.2035 11.9998C11.2044 11.8443 11.174 11.6901 11.1141 11.5461C11.0542 11.4021 10.9659 11.2712 10.8544 11.1607C10.7374 11.0563 10.603 10.9724 10.4572 10.9125ZM19.6294 8.38346C19.6168 8.27489 19.5926 8.16793 19.5572 8.06437V7.958C19.4993 7.83649 19.4221 7.72479 19.3285 7.6271L12.1062 0.536186C12.0067 0.44426 11.893 0.368464 11.7692 0.311641H11.6488C11.5318 0.25019 11.406 0.206367 11.2757 0.181641H3.98123C3.0235 0.181641 2.105 0.555179 1.42779 1.22008C0.750573 1.88498 0.370117 2.78678 0.370117 3.7271V20.2726C0.370117 21.2129 0.750573 22.1147 1.42779 22.7796C2.105 23.4445 3.0235 23.818 3.98123 23.818H16.0183C16.976 23.818 17.8945 23.4445 18.5717 22.7796C19.2489 22.1147 19.6294 21.2129 19.6294 20.2726V8.45437C19.6294 8.45437 19.6294 8.45437 19.6294 8.38346ZM12.4072 4.21164L15.5247 7.27255H13.6109C13.2916 7.27255 12.9854 7.14804 12.7597 6.9264C12.534 6.70477 12.4072 6.40417 12.4072 6.09073V4.21164ZM17.222 20.2726C17.222 20.586 17.0952 20.8866 16.8694 21.1082C16.6437 21.3299 16.3375 21.4544 16.0183 21.4544H3.98123C3.66199 21.4544 3.35582 21.3299 3.13008 21.1082C2.90434 20.8866 2.77752 20.586 2.77752 20.2726V3.7271C2.77752 3.41366 2.90434 3.11306 3.13008 2.89142C3.35582 2.66979 3.66199 2.54528 3.98123 2.54528H9.99975V6.09073C9.99975 7.03104 10.3802 7.93285 11.0574 8.59775C11.7346 9.26265 12.6531 9.63619 13.6109 9.63619H17.222V20.2726Z"
                                                                            fill="#3D2B55"/>
                                                                    </svg>
                                                                </div>
                                                                <div className="textInfo" style={{lineHeight: '10px'}}>
                                                                    <p>Правила<br/>UNO</p></div>
                                                            </button>
                                                            <button className="flex flex-col items-center focus:outline-none mt-1 ml-6"
                                                                    id="rulesRoom"
                                                                    onClick={() => {
                                                                        setShowModalRoom(true);
                                                                    }}>
                                                                <div className="flex justify-end">
                                                                    <svg className="iconBook" width="24" height="20" viewBox="0 0 24 20" fill="none"
                                                                         xmlns="http://www.w3.org/2000/svg">
                                                                        <path
                                                                            d="M7.96719 1.06784C6.42158 0.359468 4.26123 0.0124697 1.36894 0.000150264C1.09616 -0.00340715 0.828741 0.0729455 0.602333 0.219026C0.416495 0.339605 0.264371 0.502172 0.159341 0.692429C0.0543116 0.882686 -0.000405701 1.0948 2.26467e-06 1.31012V13.0588C2.26467e-06 13.853 0.588644 14.4521 1.36894 14.4521C4.40925 14.4521 7.45898 14.7248 9.28565 16.3822C9.31063 16.405 9.34204 16.4202 9.37597 16.426C9.4099 16.4318 9.44485 16.4278 9.47648 16.4147C9.50811 16.4016 9.53502 16.3798 9.55388 16.3521C9.57273 16.3245 9.5827 16.2921 9.58253 16.259V2.41559C9.58261 2.32219 9.5618 2.22986 9.5215 2.14485C9.4812 2.05984 9.42236 1.98412 9.34896 1.92281C8.93054 1.57944 8.46576 1.29185 7.96719 1.06784ZM19.9317 0.217794C19.7052 0.0720771 19.4377 -0.00384928 19.1651 0.000150264C16.2728 0.0124697 14.1124 0.357826 12.5668 1.06784C12.0683 1.29145 11.6034 1.57847 11.1846 1.92117C11.1114 1.98258 11.0527 2.05833 11.0125 2.14333C10.9722 2.22832 10.9515 2.3206 10.9515 2.41395V16.2582C10.9514 16.29 10.9612 16.321 10.9795 16.3475C10.9978 16.374 11.0239 16.3947 11.0544 16.407C11.0849 16.4194 11.1186 16.4228 11.1511 16.4168C11.1836 16.4108 11.2136 16.3958 11.2372 16.3736C12.3354 15.3264 14.2626 14.4509 19.1668 14.4513C19.5298 14.4513 19.878 14.3129 20.1348 14.0664C20.3915 13.82 20.5357 13.4858 20.5357 13.1372V1.31053C20.5362 1.09479 20.4814 0.882243 20.376 0.691666C20.2707 0.501089 20.1181 0.338342 19.9317 0.217794Z"
                                                                            fill="#DDE2E5"/>
                                                                        <path
                                                                            d="M11.5887 12.7283H11.5886L11.5887 12.7349C11.5894 12.8584 11.5948 12.9818 11.605 13.1048L10.4914 13.9539L10.4913 13.9537L10.4812 13.9618C10.3309 14.0827 10.227 14.254 10.1918 14.4466C10.1566 14.639 10.193 14.8366 10.2928 15.0035C10.293 15.0038 10.2932 15.0041 10.2933 15.0044L11.5594 17.133L11.5593 17.133L11.5624 17.138C11.663 17.3027 11.8182 17.4247 11.9981 17.4858C12.1778 17.5469 12.3735 17.5442 12.5516 17.4783L12.5516 17.4784L12.5602 17.475L13.8838 16.9572C14.1009 17.1028 14.3286 17.2324 14.5651 17.3452L14.7611 18.7002L14.7623 18.7089L14.7639 18.7175C14.7983 18.9077 14.899 19.0777 15.0453 19.1999C15.1914 19.3217 15.3744 19.3889 15.5627 19.3926L15.5627 19.3927H15.5725H18.1063V19.3928L18.1158 19.3926C18.3001 19.3891 18.4794 19.325 18.6242 19.2082C18.7694 19.0912 18.8718 18.9277 18.9117 18.7429L18.9153 18.7261L18.9178 18.709L19.1136 17.3552C19.351 17.2428 19.5793 17.1127 19.7965 16.9661L21.1187 17.4833L21.1186 17.4834L21.127 17.4865C21.3051 17.5525 21.5007 17.5553 21.6804 17.4944C21.8603 17.4334 22.0155 17.3115 22.1162 17.1469L22.1163 17.1469L22.1195 17.1416L23.3855 15.0133C23.3856 15.0131 23.3857 15.0129 23.3858 15.0128C23.4858 14.8458 23.5223 14.648 23.4871 14.4554C23.4519 14.2628 23.348 14.0915 23.1976 13.9707L23.1977 13.9705L23.1875 13.9627L22.0733 13.1132C22.0836 12.9876 22.0893 12.8617 22.0902 12.7356L22.0902 12.7356L22.0902 12.7291C22.0895 12.6056 22.084 12.4822 22.0738 12.3592L23.1875 11.5102L23.1876 11.5103L23.1976 11.5022C23.348 11.3813 23.4519 11.21 23.4871 11.0174C23.5222 10.8249 23.4858 10.6272 23.3859 10.4602C23.3858 10.46 23.3857 10.4598 23.3855 10.4596L22.1195 8.33105L22.1195 8.33103L22.1165 8.32602C22.0158 8.16127 21.8607 8.03927 21.6807 7.97817C21.501 7.91715 21.3053 7.91976 21.1273 7.98569L21.1272 7.98561L21.1187 7.98896L19.7951 8.50678C19.578 8.36125 19.3502 8.23159 19.1137 8.11883L18.9178 6.76383L18.9165 6.75513L18.915 6.74648C18.8806 6.55626 18.7799 6.38626 18.6335 6.26415C18.4874 6.14227 18.3044 6.07507 18.1161 6.07138L18.1161 6.07129H18.1063H15.5725V6.0712L15.563 6.07138C15.3788 6.07487 15.1995 6.13904 15.0546 6.25584C14.9095 6.37284 14.807 6.53634 14.7672 6.72109L14.7635 6.73794L14.7611 6.75499L14.5653 8.10856C14.3275 8.221 14.0988 8.35116 13.8812 8.49793L12.5603 7.98071L12.5604 7.98063L12.5518 7.97746C12.3738 7.91147 12.1782 7.90876 11.9985 7.96966C11.8185 8.03062 11.6633 8.15247 11.5626 8.3171L11.5626 8.31708L11.5594 8.32247L10.2933 10.451C10.2931 10.4513 10.293 10.4516 10.2928 10.4519C10.193 10.6188 10.1566 10.8164 10.1918 11.0088C10.227 11.2015 10.3309 11.3727 10.4812 11.4936L10.4811 11.4938L10.4914 11.5016L11.6056 12.3511C11.5952 12.4765 11.5896 12.6024 11.5887 12.7283ZM16.8402 11.0891C17.1792 11.0893 17.5094 11.1873 17.7892 11.3694C18.0691 11.5515 18.2852 11.809 18.4123 12.1077C18.5393 12.4062 18.5723 12.734 18.5077 13.05C18.4431 13.3661 18.2834 13.6584 18.0465 13.889C17.8095 14.1198 17.506 14.2784 17.1736 14.3428C16.8412 14.4072 16.4968 14.374 16.1844 14.248C15.8721 14.1221 15.6074 13.9097 15.4221 13.6396C15.2371 13.37 15.1391 13.0544 15.139 12.7328C15.1405 12.3016 15.3169 11.8858 15.6342 11.5769C15.952 11.2674 16.3853 11.0906 16.8402 11.0891Z"
                                                                            fill="#DDE2E5" stroke="#3D2B55"/>
                                                                    </svg>

                                                                </div>
                                                                <div className="textBook" style={{lineHeight: '10px'}}>
                                                                    <p className="align-center">Правила<br/>комнаты</p></div>
                                                            </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        : null
                                                    }
                                                </div>

                                            </div>
                                            : null}
                                        </div>);
                                })}

                                { (waitStartGame && players.length !== 4) ?
                                    <div
                                        className={`row-start-2 col-start-3 col-span-1 flex justify-start sideClip`}>
                                        <div className="flex playerOnTable justify-center items-center">
                                            <div className="flex flex-col flex-grow-0 justify-center items-center">
                                                <div
                                                    className="relative avatarNone tablet:rounded-2.5xl tablet:border-5 tablet:w-15 tablet:h-15 mobile:rounded-2xl mobile:border-4 mobile:w-10 mobile:h-10 tablet:mb-2.5 mobile:mb-2">
                                                </div>
                                                <div className="nickNone tablet:w-20 mobile:w-12 rounded-lg">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }

                                { waitStartGame && players.length !== 4 && players.length !== 3?
                                    <div
                                        className={`row-start-1 col-start-2 col-span-1 h-full flex items-end justify-center`}>
                                        <div className="flex flex-col justify-center items-center">
                                            <div
                                                className="relative avatarNone tablet:rounded-2.5xl tablet:border-5 tablet:w-15 tablet:h-15 mobile:rounded-2xl mobile:border-4 mobile:w-10 mobile:h-10 tablet:mb-2.5 mobile:mb-2">
                                            </div>
                                            <div className="nickNone tablet:w-20 mobile:w-12 rounded-lg">
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }

                                { waitStartGame && players.length !== 4 && players.length !== 3 && players.length !== 2
                                    ?
                                    <div
                                        className={`row-start-2 col-start-1 col-span-1 flex justify-end sideClip`}>
                                        <div className="flex playerOnTable justify-center items-center">
                                            <div className="flex flex-col flex-grow-0 justify-center items-center">
                                                <div
                                                    className="relative avatarNone tablet:rounded-2.5xl tablet:border-5 tablet:w-15 tablet:h-15 mobile:rounded-2xl mobile:border-4 mobile:w-10 mobile:h-10 tablet:mb-2.5 mobile:mb-2">
                                                </div>
                                                <div className="nickNone tablet:w-20 mobile:w-12 rounded-lg">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }


                                <div
                                    className={`row-start-2 col-span-3 
                                    mobile:row-start-2 mobile:col-start-2 mobile:col-span-1 
                                    lg:px-20 py-4 
                                    flex flex-col 
                                    tablet:justify-center mobile:justify-end items-center
                                    tablet:h-auto mobile:h-full`}
                                >
                                    {waitStartGame ?
                                        playersActive.map((player) => {
                                            const isAdmin =
                                                player.data().admin == true && player.id == playerId;
                                            const user = player.data().admin == false && player.id == playerId;
                                            return isAdmin ? (
                                                    <div className="flex flex-no-wrap justify-center items-center">
                                                        <div><Button
                                                            key={player.id}
                                                            ariaLabel="Start"
                                                            color="green"
                                                            onClick={onNewGame}
                                                            className="btnGreen tablet:py-4.5 tablet:px-14 mobile:py-3 mobile:px-8 rounded-def font-bold tablet:text-3xl mobile:text-xl leading-btn"
                                                            disabled={!isAdmin || (playersActive.length == 1)}
                                                            onBlur={" "}
                                                        >
                                                            Старт
                                                        </Button></div>
                                                    </div>
                                                ) :
                                                user ?
                                                    <div className="h-full flex justify-center items-center"><p
                                                        className="font-bold text-white tablet:text-4.5xl mobile:text-2xl text-center leading-12">
                                                        Ожидаем<br/>старта игры...</p></div> : null;
                                        })
                                        :
                                        <div className="h-full flex justify-center items-center"><p
                                            className="font-bold text-white tablet:text-4.5xl mobile:text-2xl text-center leading-12">
                                            Ожидаем<br/>других игроков...</p></div>}
                                </div>
                            </div>
                        </div>
                    </div>
                    <ModalRules
                        className="modalRules"
                        contentLabel="rulesGame"
                        id="modal-rules-game"
                        isOpen={showModalGame}
                        onRequestClose={() => {
                            setShowModalGame(false);
                        }}
                        overlayClassName="overlay overlayModalInGame"
                        title="Правила игры UNO"
                    >
                        <Rules onClick={() => {
                            setShowModalGame(false);
                            setShowModalRoom(true);
                        }} onRequestClose={() => {
                            setShowModalGame(false);
                        }}/>
                    </ModalRules>
                    <ModalRules
                        className="modalRules"
                        contentLabel="rulesRoom"
                        id="modal-rules-room"
                        isOpen={showModalRoom}
                        onRequestClose={() => {
                            setShowModalRoom(false);
                        }}
                        overlayClassName="overlay overlayModalInGame"
                        title="Правила комнаты"
                        isAdmin={!!adminPlayer}
                    >
                        <SpecialRules admin={adminPlayer} roomId={roomId} room={room} roomRef={roomRef} onRequestClose={() => {
                            setShowModalRoom(false);
                        }}/>
                    </ModalRules>
                </Layout>
                <style jsx>{`
                .avatarNone {
                    background: #DDE2E5;
                    border-color: #C4C4C4;
                    box-shadow: 0px 8px 10px -6px rgba(25, 10, 47, 0.3), 0px 2px 10px -4px rgba(25, 10, 47, 0.3);
                }
                
                .nickNone {
                    height: 1.1875rem;
                    background: rgba(196, 196, 196, 0.2);
                }
                
                `}</style>
            </Main>
        );
    }
};

export default Game;
