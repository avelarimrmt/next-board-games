import Head from 'next/head'
import Link from "next/link";
import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import db from "../../../config/firebase";
import Layout from "../../../components/Layout";
import Heading from "../../../components/Heading";
import styles from '../../../styles/Lobby.module.css'
import {wildFourDifficultyTitle} from "../../../utils/unoSpecialRules";

const Lobby: React.FC = () => {
    const router = useRouter();
    const [rooms, setRooms] = useState([]);
    const roomsRef = db.collection("rooms");

    useEffect(() => {
        const dataRoom = roomsRef
            .onSnapshot(function (querySnapshot) {
                let rooms = [];
                querySnapshot.forEach(function (roomsRef) {
                    rooms.push(roomsRef.data());
                });
                setRooms(rooms);
            });

        return () => {
            dataRoom();
        };
    });

    let playerName;
    if (typeof window !== 'undefined') {
        playerName = sessionStorage.getItem('name');
    }
    const [room, setRoom] = useState("");

    const randomAvatar = () => {
        const min = 1;
        const max = 28;
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        if (room) {
            Promise.all([roomsRef.doc(room).get(), roomsRef.doc(room).collection("players").get()]).then(
                ([roomSnapshot, playersSnapshot]) => {
                    if (roomSnapshot.data().count == 0) {
                        roomsRef.doc(room).set({
                            count: 1,
                            admin: playerName,
                            maxCount: 4,
                            playing: false,
                            number: 7,
                            hintsWhatCards: true,
                            hintsForBtn: false,
                            wildFourDifficulty: false,
                            unoToTheLast: false,
                            dropSameCards: false,
                            accum: false,
                            winners: [],
                        }, {merge: true}).then(() => {
                            roomsRef.doc(room).collection("players")
                                .add({name: playerName, admin: true, cards: [], winner: false, avatar: randomAvatar(), isDrop: false, isExit: false})
                                .then(
                                    (playerRef) => {
                                        router.push(
                                            "/uno/rooms/[roomId]/players/[playersId]",
                                            `/uno/rooms/${room}/players/${playerRef.id}`
                                        );
                                    });
                        });
                    } else {
                        roomsRef.doc(room).set({count: (playersSnapshot.size + 1)}, {merge: true}).then(() => {
                            roomsRef.doc(room).collection("players")
                                .add({name: playerName, admin: false, number: playersSnapshot.size, cards: [], winner: false, avatar: randomAvatar(), isDrop: false, isExit: false})
                                .then(
                                    (playerRef) => {
                                        router.push(
                                            "/uno/rooms/[roomId]/players/[playersId]",
                                            `/uno/rooms/${room}/players/${playerRef.id}`
                                        );
                                    });
                        });
                    }
                });
        }
    }

    if (!rooms) {
        return (
            <main className="h-screen w-full bg-blue-dark">
                <Layout room={null}>
                    <Heading color="white">
                        Loading...
                    </Heading>
                </Layout>
            </main>
        );
    } else {
        const roomSlots = [];
        for (let i = 0; i < rooms.length; i++) {
            const room = rooms[i];
            roomSlots.push(
                <tr className={`${styles.tr}`}>
                    {room.admin ?
                        <td className={`${styles.td} ${styles.td1} text-base text-purple font-extrabold`}>{room.admin}</td> :
                        <td className={`${styles.td} ${styles.td1} lato tablet:text-base mobile:text-xs text-black-light font-medium`}>
                            Комната пуста. Присоединяйтесь прямо сейчас!
                        </td>}

                    <td className={`${styles.td} lato tablet:text-xl mobile:text-base font-extrabold`}>
                        {(room.count === room.maxCount) ?
                            <div className="float-right text-red">{room.count}/{room.maxCount}</div> :
                            <div className="float-right">{room.count}/{room.maxCount}</div>}
                    </td>

                    {room.playing ?
                        <td className={`${styles.playing} ${styles.td}`}>
                            <div className="float-right"><img src="/uno/playing.svg" alt=""/></div>
                            <div className={`${styles.holder} lato text-xs text-red p-6`}>
                                Пока в комнате идет партия, в нее нельзя зайти
                            </div>
                        </td> : <td className={`${styles.td}`}></td>}

                    <td className={`${styles.td} ${styles.td4}`}>
                        <div className="float-right">
                            <button type="submit"
                                    className={`btn ${styles.btnLogin} btnGreen bg-cyan active:bg-cyan-active text-white font-bold text-sm`}
                                    onClick={() => setRoom("room" + room.id.toString())}
                                    disabled={(room.playing === true) || (room.count === room.maxCount)}>
                                Войти
                            </button>
                        </div>
                    </td>
                </tr>
            );
        }
        return (
            <>
                <Head>
                    <title>Доступные комнаты</title>
                </Head>
                <main className="h-screen w-full bg-blue-dark">
                    <div className="container2 flex flex-col mobile:relative ">
                        <div className="tablet:hidden mobile:block mobile:transform mobile:rotate-180 absolute" style={{top: '40px', left: '0px', }}>
                            <Link href="/uno">
                                <svg className="cursor-pointer exitLobbyAndRoom" width="22" height="28" viewBox="0 0 22 28"
                                     fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.729492 14.0947C0.729492 14.4528 0.867773 14.7961 1.11391 15.0493C1.36006 15.3025 1.6939 15.4447 2.04199 15.4447H12.0039L8.98512 18.5362C8.8621 18.6617 8.76446 18.811 8.69782 18.9755C8.63119 19.1401 8.59688 19.3165 8.59688 19.4947C8.59688 19.6729 8.63119 19.8494 8.69782 20.0139C8.76446 20.1784 8.8621 20.3277 8.98512 20.4532C9.10713 20.5798 9.25229 20.6802 9.41224 20.7487C9.57218 20.8173 9.74373 20.8526 9.91699 20.8526C10.0903 20.8526 10.2618 20.8173 10.4217 20.7487C10.5817 20.6802 10.7269 20.5798 10.8489 20.4532L16.0989 15.0532C16.2184 14.9248 16.312 14.7734 16.3745 14.6077C16.5058 14.2791 16.5058 13.9104 16.3745 13.5817C16.312 13.416 16.2184 13.2646 16.0989 13.1362L10.8489 7.73623C10.7265 7.61035 10.5812 7.51051 10.4213 7.44239C10.2614 7.37426 10.0901 7.3392 9.91699 7.3392C9.74393 7.3392 9.57256 7.37426 9.41266 7.44239C9.25277 7.51051 9.10749 7.61035 8.98512 7.73623C8.86274 7.8621 8.76567 8.01153 8.69944 8.17599C8.63321 8.34045 8.59912 8.51672 8.59912 8.69473C8.59912 8.87274 8.63321 9.049 8.69944 9.21346C8.76567 9.37792 8.86274 9.52736 8.98512 9.65323L12.0039 12.7447H2.04199C1.6939 12.7447 1.36006 12.887 1.11391 13.1401C0.867773 13.3933 0.729492 13.7367 0.729492 14.0947ZM17.792 0.594727H4.66699C3.6227 0.594727 2.62118 1.02142 1.88276 1.78094C1.14433 2.54047 0.729492 3.5706 0.729492 4.64473V8.69473C0.729492 9.05277 0.867773 9.39615 1.11391 9.64932C1.36006 9.90249 1.6939 10.0447 2.04199 10.0447C2.39009 10.0447 2.72393 9.90249 2.97007 9.64932C3.21621 9.39615 3.35449 9.05277 3.35449 8.69473V4.64473C3.35449 4.28668 3.49277 3.94331 3.73891 3.69013C3.98506 3.43696 4.3189 3.29473 4.66699 3.29473H17.792C18.1401 3.29473 18.4739 3.43696 18.7201 3.69013C18.9662 3.94331 19.1045 4.28668 19.1045 4.64473V23.5447C19.1045 23.9028 18.9662 24.2461 18.7201 24.4993C18.4739 24.7525 18.1401 24.8947 17.792 24.8947H4.66699C4.3189 24.8947 3.98506 24.7525 3.73891 24.4993C3.49277 24.2461 3.35449 23.9028 3.35449 23.5447V19.4947C3.35449 19.1367 3.21621 18.7933 2.97007 18.5401C2.72393 18.287 2.39009 18.1447 2.04199 18.1447C1.6939 18.1447 1.36006 18.287 1.11391 18.5401C0.867773 18.7933 0.729492 19.1367 0.729492 19.4947V23.5447C0.729492 24.6189 1.14433 25.649 1.88276 26.4085C2.62118 27.168 3.6227 27.5947 4.66699 27.5947H17.792C18.8363 27.5947 19.8378 27.168 20.5762 26.4085C21.3146 25.649 21.7295 24.6189 21.7295 23.5447V4.64473C21.7295 3.5706 21.3146 2.54047 20.5762 1.78094C19.8378 1.02142 18.8363 0.594727 17.792 0.594727Z"
                                        fill="white"/>
                                </svg>
                            </Link>
                        </div>

                        <div className="flex flex-row justify-between mt-24 items-center relative">
                            <h1 className="lato tablet:text-3xl mobile:text-2xl leading-8 text-white"><span
                                className="nunito font-extrabold">UNO: </span> Доступные комнаты</h1>
                            <div className="tablet:block mobile:hidden">
                                <Link href="/uno">
                                    <svg className="cursor-pointer exitLobbyAndRoom" width="22" height="28" viewBox="0 0 22 28"
                                         fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M0.729492 14.0947C0.729492 14.4528 0.867773 14.7961 1.11391 15.0493C1.36006 15.3025 1.6939 15.4447 2.04199 15.4447H12.0039L8.98512 18.5362C8.8621 18.6617 8.76446 18.811 8.69782 18.9755C8.63119 19.1401 8.59688 19.3165 8.59688 19.4947C8.59688 19.6729 8.63119 19.8494 8.69782 20.0139C8.76446 20.1784 8.8621 20.3277 8.98512 20.4532C9.10713 20.5798 9.25229 20.6802 9.41224 20.7487C9.57218 20.8173 9.74373 20.8526 9.91699 20.8526C10.0903 20.8526 10.2618 20.8173 10.4217 20.7487C10.5817 20.6802 10.7269 20.5798 10.8489 20.4532L16.0989 15.0532C16.2184 14.9248 16.312 14.7734 16.3745 14.6077C16.5058 14.2791 16.5058 13.9104 16.3745 13.5817C16.312 13.416 16.2184 13.2646 16.0989 13.1362L10.8489 7.73623C10.7265 7.61035 10.5812 7.51051 10.4213 7.44239C10.2614 7.37426 10.0901 7.3392 9.91699 7.3392C9.74393 7.3392 9.57256 7.37426 9.41266 7.44239C9.25277 7.51051 9.10749 7.61035 8.98512 7.73623C8.86274 7.8621 8.76567 8.01153 8.69944 8.17599C8.63321 8.34045 8.59912 8.51672 8.59912 8.69473C8.59912 8.87274 8.63321 9.049 8.69944 9.21346C8.76567 9.37792 8.86274 9.52736 8.98512 9.65323L12.0039 12.7447H2.04199C1.6939 12.7447 1.36006 12.887 1.11391 13.1401C0.867773 13.3933 0.729492 13.7367 0.729492 14.0947ZM17.792 0.594727H4.66699C3.6227 0.594727 2.62118 1.02142 1.88276 1.78094C1.14433 2.54047 0.729492 3.5706 0.729492 4.64473V8.69473C0.729492 9.05277 0.867773 9.39615 1.11391 9.64932C1.36006 9.90249 1.6939 10.0447 2.04199 10.0447C2.39009 10.0447 2.72393 9.90249 2.97007 9.64932C3.21621 9.39615 3.35449 9.05277 3.35449 8.69473V4.64473C3.35449 4.28668 3.49277 3.94331 3.73891 3.69013C3.98506 3.43696 4.3189 3.29473 4.66699 3.29473H17.792C18.1401 3.29473 18.4739 3.43696 18.7201 3.69013C18.9662 3.94331 19.1045 4.28668 19.1045 4.64473V23.5447C19.1045 23.9028 18.9662 24.2461 18.7201 24.4993C18.4739 24.7525 18.1401 24.8947 17.792 24.8947H4.66699C4.3189 24.8947 3.98506 24.7525 3.73891 24.4993C3.49277 24.2461 3.35449 23.9028 3.35449 23.5447V19.4947C3.35449 19.1367 3.21621 18.7933 2.97007 18.5401C2.72393 18.287 2.39009 18.1447 2.04199 18.1447C1.6939 18.1447 1.36006 18.287 1.11391 18.5401C0.867773 18.7933 0.729492 19.1367 0.729492 19.4947V23.5447C0.729492 24.6189 1.14433 25.649 1.88276 26.4085C2.62118 27.168 3.6227 27.5947 4.66699 27.5947H17.792C18.8363 27.5947 19.8378 27.168 20.5762 26.4085C21.3146 25.649 21.7295 24.6189 21.7295 23.5447V4.64473C21.7295 3.5706 21.3146 2.54047 20.5762 1.78094C19.8378 1.02142 18.8363 0.594727 17.792 0.594727Z"
                                            fill="white"/>
                                    </svg>
                                </Link>
                            </div>
                        </div>
                        <div className="rooms flex w-full tablet:mt-10.5 mobile:mt-4">
                            <div className="flex-row w-full">
                                <form onSubmit={handleSubmit}>
                                    {roomSlots[0] != null ?
                                        <table className={`${styles.table} w-full`}>
                                        <thead className={`${styles.thead} lato leading-8 tablet:text-sm mobile:text-xs font-medium 
                                        tablet:text-gray-light mobile:text-gray-dark`}>
                                        <tr className={`${styles.td}`}>
                                            <td className={`pl-5 ${styles.td}`}>Администратор комнаты</td>
                                            <td className={`${styles.td}`}>Кол-во игроков</td>
                                        </tr>
                                        </thead>
                                        <tbody className={`${styles.tbody}`}>
                                        {roomSlots}
                                        </tbody>
                                    </table>
                                    : <Heading className="w-full" color="white">
                                            Loading...
                                        </Heading>
                                    }
                                </form>
                            </div>
                        </div>
                    </div>
                </main>
            </>
        )
    }
};

export default Lobby;
