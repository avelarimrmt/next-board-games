import React, { Fragment } from "react";
import App from "next/app";
import Head from "next/head";
import '../styles/globals.css'

export default class UnoGame extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
        <Fragment>
          <Head>
            <link rel="icon" href="/favicon.ico" />
          </Head>
          <Component {...pageProps} />
        </Fragment>
    );
  }
}
