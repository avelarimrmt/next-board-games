import Document, {Html, Head, Main, NextScript} from 'next/document'

class MyDocument extends Document {

    render() {
        return (
            <Html>
                <Head>
                    <link rel="preconnect" href="https://fonts.gstatic.com"/>
                    <link href="/manifest.json" rel="manifest"/>
                    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700;800;900&display=swap"
                          rel="stylesheet"/>
                    <link href="/utils/fonts/lato/stylesheet.css" rel="stylesheet" type="text/css"/>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        )
    }
}

export default MyDocument