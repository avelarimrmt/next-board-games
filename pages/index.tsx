import Link from 'next/link'
import {MainLayout} from "../components/MainLayout";
import React from "react";
import styles from '../styles/Home.module.css'
import Carousel, { Dots, slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

class Home extends React.Component {
    componentDidMount() {
        document.body.classList.add('homePage');
    }

    componentWillUnmount() {
        document.body.classList.remove('homePage');
    }

    render() {
        return (
            <MainLayout>
                <main className="w-full flex-col">
                    <div className="container">
                        <div className={`${styles.content} w-full`}>
                            <div className="w-full flex flex-col justify-start items-start">
                                <h1 className="font-extrabold text-sm text-gray-light tablet:text-5xl tablet:pt-24 pt-10 pb-4 tablet:pb-4 tablet:text-blue">
                                    Добро пожаловать!
                                </h1>
                                <p className="lato font-normal text-black-light pb-16 mobile:hidden tablet:flex">
                                    Мы рады приветствовать вас на нашем игровом портале.<br/>
                                    Здесь вы сможете весело провести время с друзьями за вашими<br/>
                                    любимыми настольными играми.
                                </p>
                                <div className="w-full mobile:hidden tablet:flex">
                                    <div className={`${styles.descCard1} bg-yellow flex flex-row`}>
                                        <div className="flex h-100 w-1/3 justify-center items-center">
                                            <div className="flex"><img src="people.svg" alt=""/></div>
                                        </div>
                                        <div className="flex w-2/3 items-center justify-start">
                                            <p className="flex font-bold text-sm text-white m-auto pr-5">
                                                Виртуальные комнаты, где можно играть как с друзьями,
                                                так и со случайными игроками
                                            </p>
                                        </div>
                                    </div>
                                    <div className={`${styles.descCard2} bg-red flex flex-row`}>
                                        <div className="flex h-100 w-1/3 items-center justify-center">
                                            <div className="flex"><img src="settings.svg" alt=""/></div>
                                        </div>
                                        <div className="flex w-3/5 items-center justify-start">
                                            <p className="flex font-bold text-sm text-white m-auto pr-5">
                                                Настройка правил в игровых комнатах
                                                для более комфортной игры
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container">
                        <div className={`w-full flex flex-col ${styles.games}`}>
                            <div className="w-full flex justify-start">
                                <h2 className="font-extrabold tablet:pt-4 pb-8 text-red text-2rem">
                                    Игры
                                </h2>
                            </div>
                            <div className="w-full mobile:hidden desktop:flex">
                                <div className={`w-full flex flex-row ${styles.cards} justify-between`}>
                                    <div
                                        className={`flex flex-col flex-end ${styles.cardGame} ${styles.cardGame1} bg-red pl-6 pt-10 pb-6 pr-4`}>

                                        <div className={`${styles.svgCard} ${styles.iconUno}`}>
                                        </div>

                                        <div className={`${styles.numberOfPlayers} flex flex-row`}>
                                            <img className="h-100" src="players.svg" alt=""/>
                                            <div className="h-100 flex pl-2">
                                                <p className="lato text-sm m-auto text-pink-light">2-10</p>
                                            </div>
                                        </div>

                                        <div className={`${styles.difficultOfGame}`}>
                                            <p className="lato text-sm text-pink-light">Сложность</p>
                                            <div className="flex flex-row">
                                            <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                <span className={`${styles.star1}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                            </div>
                                        </div>

                                        <div className={`${styles.nameOfGame} font-extrabold text-white text-5xl`}>
                                            UNO
                                        </div>

                                        <div className="flex justify-end items-end">
                                            <Link href="/uno">
                                                <button type="button"
                                                        className={`btn ${styles.btnPlay1} bg-blue hover:bg-blue-hover 
                                                        active:bg-blue-active text-white font-bold text-sm`}>
                                                    Играть
                                                </button>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className={`relative`}>
                                        <div
                                            className={`flex flex-col flex-end ${styles.cardGame} ${styles.cardGame2} bg-blue pl-6 pt-10 pb-6 opacity-30`}>

                                            <div className={`${styles.svgCard}`}>
                                            </div>

                                            <div className={`${styles.numberOfPlayers} flex flex-row`}>
                                                <img className="h-100" src="players.svg" alt=""/>
                                                <div className="h-100 flex pl-2">
                                                    <p className="lato text-sm m-auto text-blue-light">2-10</p>
                                                </div>
                                            </div>

                                            <div className={`${styles.difficultOfGame}`}>
                                                <p className="lato text-sm text-blue-light">Сложность</p>
                                                <div className="flex flex-row">
                                            <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                    <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                    <span className={`${styles.star1}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                </div>
                                            </div>

                                            <div className={`${styles.nameOfGame}  text-white font-extrabold`}
                                                 style={{fontSize: '2.25rem'}}>
                                                Преферанс
                                            </div>
                                        </div>
                                        <div
                                            className={`flex ${styles.intransparent} absolute w-full justify-end items-end `}>
                                            <div
                                                className={`flex ${styles.flag} ${styles.flagRed} items-center justify-end pr-4`}>
                                                <span className="lato text-lg text-white">Скоро!</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="relative">
                                        <div
                                            className={`flex flex-col flex-end ${styles.cardGame} ${styles.cardGame3} bg-yellow pl-6 pt-10 pb-6 opacity-30`}>
                                            <div className={`${styles.svgCard}`}>
                                            </div>

                                            <div className={`${styles.numberOfPlayers} flex flex-row`}>
                                                <img className="h-100" src="players.svg" alt=""/>
                                                <div className="h-100 flex pl-2">
                                                    <p className="lato text-sm m-auto text-pink-light">2-10</p>
                                                </div>
                                            </div>

                                            <div className={`${styles.difficultOfGame}`}>
                                                <p className="lato text-sm text-pink-light">Сложность</p>

                                                <div className="flex flex-row">
                                                    <span className={`${styles.star2} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                    <span className={`${styles.star2} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                    <span className={`${styles.star2}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                </div>
                                            </div>

                                            <div className={`${styles.nameOfGame} font-extrabold text-white text-5xl`}>
                                                Мафия
                                            </div>
                                        </div>

                                        <div className={`flex ${styles.intransparent} absolute w-full justify-end items-end`}>
                                            <div
                                                className={`flex ${styles.flag} ${styles.flagBlue} items-center justify-end pr-4`}>
                                                <span className="lato text-lg text-white">Скоро!</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Carousel
                                plugins={[
                                    'fastSwipe',
                                    {
                                        resolve: slidesToShowPlugin,
                                        options: {
                                            numberOfSlides: 2
                                        }
                                    },
                                ]}
                            >
                                <div
                                    className={`flex mb-6 desktop:hidden flex-col flex-end ${styles.cardGame} ${styles.cardGame1} bg-red pl-6 pt-10 pb-6 pr-4`}>

                                    <div className={`${styles.svgCard} ${styles.iconUno}`}>
                                    </div>

                                    <div className={`${styles.numberOfPlayers} flex flex-row`}>
                                        <img className="h-100" src="players.svg" alt=""/>
                                        <div className="h-100 flex pl-2">
                                            <p className="lato text-sm m-auto text-pink-light">2-10</p>
                                        </div>
                                    </div>

                                    <div className={`${styles.difficultOfGame}`}>
                                        <p className="lato text-sm text-pink-light">Сложность</p>
                                        <div className="flex flex-row">
                                            <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                            <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                            <span className={`${styles.star1}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                        </div>
                                    </div>

                                    <div className={`${styles.nameOfGame} font-extrabold text-white text-5xl`}>
                                        UNO
                                    </div>

                                    <div className="flex justify-end items-end">
                                        <Link href="/uno">
                                            <button type="button"
                                                    className={`btn ${styles.btnPlay1} bg-blue hover:bg-blue-hover 
                                                        active:bg-blue-active text-white font-bold text-sm`}>
                                                Играть
                                            </button>
                                        </Link>
                                    </div>
                                </div>

                                <div className={`relative mb-6 desktop:hidden`}>
                                    <div
                                        className={`flex flex-col flex-end ${styles.cardGame} ${styles.cardGame2} bg-blue pl-6 pt-10 pb-6 opacity-30`}>

                                        <div className={`${styles.svgCard}`}>
                                        </div>

                                        <div className={`${styles.numberOfPlayers} flex flex-row`}>
                                            <img className="h-100" src="players.svg" alt=""/>
                                            <div className="h-100 flex pl-2">
                                                <p className="lato text-sm m-auto text-blue-light">2-10</p>
                                            </div>
                                        </div>

                                        <div className={`${styles.difficultOfGame}`}>
                                            <p className="lato text-sm text-blue-light">Сложность</p>
                                            <div className="flex flex-row">
                                            <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                <span className={`${styles.star1} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                <span className={`${styles.star1}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                            </div>
                                        </div>

                                        <div className={`${styles.nameOfGame}  text-white font-extrabold`}
                                             style={{fontSize: '2.25rem'}}>
                                            Преферанс
                                        </div>
                                    </div>
                                    <div
                                        className={`flex ${styles.intransparent} absolute w-full justify-end items-end `}>
                                        <div
                                            className={`flex ${styles.flag} ${styles.flagRed} items-center justify-end pr-4`}>
                                            <span className="lato text-lg text-white">Скоро!</span>
                                        </div>
                                    </div>
                                </div>

                                <div className="relative mb-6 desktop:hidden">
                                    <div
                                        className={`flex flex-col flex-end ${styles.cardGame} ${styles.cardGame3} bg-yellow pl-6 pt-10 pb-6 opacity-30`}>
                                        <div className={`${styles.svgCard}`}>
                                        </div>

                                        <div className={`${styles.numberOfPlayers} flex flex-row`}>
                                            <img className="h-100" src="players.svg" alt=""/>
                                            <div className="h-100 flex pl-2">
                                                <p className="lato text-sm m-auto text-pink-light">2-10</p>
                                            </div>
                                        </div>

                                        <div className={`${styles.difficultOfGame}`}>
                                            <p className="lato text-sm text-pink-light">Сложность</p>

                                            <div className="flex flex-row">
                                                    <span className={`${styles.star2} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                <span className={`${styles.star2} pr-3`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                                <span className={`${styles.star2}`}>
                                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M14.1892 5.51715L10.222 4.94058L8.44859 1.34527C8.40015 1.24683 8.32046 1.16714 8.22203 1.11871C7.97515 0.996832 7.67515 1.09839 7.55171 1.34527L5.77828 4.94058L1.81109 5.51715C1.70171 5.53277 1.60171 5.58433 1.52515 5.66246C1.43259 5.75759 1.38159 5.88558 1.38334 6.0183C1.3851 6.15103 1.43948 6.27762 1.53453 6.37027L4.40484 9.16871L3.72671 13.1203C3.71081 13.2122 3.72098 13.3067 3.75608 13.3932C3.79117 13.4796 3.84978 13.5545 3.92526 13.6093C4.00074 13.6641 4.09007 13.6967 4.18312 13.7033C4.27617 13.71 4.36922 13.6904 4.45171 13.6468L8.00015 11.7812L11.5486 13.6468C11.6455 13.6984 11.758 13.7156 11.8658 13.6968C12.1377 13.65 12.3205 13.3921 12.2736 13.1203L11.5955 9.16871L14.4658 6.37027C14.5439 6.29371 14.5955 6.19371 14.6111 6.08433C14.6533 5.81089 14.4627 5.55777 14.1892 5.51715Z"
                                                    fill="#E9A527"/>
                                            </svg>
                                            </span>
                                            </div>
                                        </div>

                                        <div className={`${styles.nameOfGame} font-extrabold text-white text-5xl`}>
                                            Мафия
                                        </div>
                                    </div>

                                    <div className={`flex ${styles.intransparent} absolute w-full justify-end items-end`}>
                                        <div
                                            className={`flex ${styles.flag} ${styles.flagBlue} items-center justify-end pr-4`}>
                                            <span className="lato text-lg text-white">Скоро!</span>
                                        </div>
                                    </div>
                                </div>

                            </Carousel>
                        </div>
                    </div>
                </main>
            </MainLayout>
        )
    }
}

export default Home;